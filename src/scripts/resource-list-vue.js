const initShowingItems = 9;
const increment = 6;

initUrlParams();

function initUrlParams() {
	window.urlParameters = {
		cap: null,
		keyword: "",
		selectedIds: []
	};

	var url = null;
	try {
		var url_string = window.location.href;
		url = new URL(url_string);
	}
	catch (err) {
		console.log(err);
	}

	if (url) {
		window.urlParameters.cap = url.searchParams.get("cap") ? url.searchParams.get("cap") : null;
		window.urlParameters.keyword = url.searchParams.get("keyword") ? url.searchParams.get("keyword").trim() : "";
		
		const selected = url.searchParams.get("selectedIds") ? url.searchParams.get("selectedIds") : null;
		if (selected) {
			window.urlParameters.selectedIds = selected.split(",")
		} else {
			window.urlParameters.selectedIds = null;
		}
	}
}

// make 'cap' always (initShowingItems + increment * x), to avoid showing weird number of items with load more button
function validateCap(rawNumber) {
	// my weird math stuff...
	var m = (rawNumber - initShowingItems) / increment;
	var x = isInt(m) ? m : Math.floor(m + 1);
	var validatedVal = increment * x + initShowingItems;
	return validatedVal;
}

Vue.component('multiselect', window.VueMultiselect.default);

$(function () {
	window.resourcesVue = new Vue({
		el: '#resource-list-vue',
		delimiters: ['{*', '*}'],
		data: {
			allResults: null,
			initErrored: false,
			selected: {
			},
			selectedIds: [
				window.urlParameters.selectedIds ? window.urlParameters.selectedIds : "",
			],
			message: {
				info: null,
				notice: null,
				error: null,
			},
			initialising: true,
			showTopTags: true,
			filters: {
			},
			keyword: window.urlParameters.keyword ? window.urlParameters.keyword : "",
			cap: window.urlParameters.cap && validateCap(window.urlParameters.cap) ? validateCap(window.urlParameters.cap) : initShowingItems,
		},
		methods: {
			filterByAttr(tag) {
				const self = this;
				
				// for the number of multiselect filters we have
				for (let i = 0; i < ObjectLength(this.filters); i++) {
					const resFilter = this.filters[i];

					// filter through the options in those filters to find the matching one that was clicked
					resFilter.options.filter ( function (option) {
						let alreadySelected = false;
						self.selected[resFilter.id].map( function (selected) {
							if (selected.id === tag.id) {
								alreadySelected = true
							} 
						});

						if(option.id === tag.id) {
							if (!alreadySelected) {
								const selectedTypeLength = ObjectLength(self.selected[resFilter.id]);
								Vue.set(self.selected[resFilter.id], selectedTypeLength, option)
							}
						}
						
					});
					var element = document.getElementById("main_banner");
					element.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" })
				}
			},
			initSelectedIds() {
				const self = this;
				const msFilters = Object.keys(this.filters)
				// search through filters and match the ID (filterID) from the URL to the selected Filter ID and then push it to the group's array
				for (var a = 0; a < msFilters.length; a++) {
					const filterItem = this.filters[a];
					filterItem.options.find( function(item) {
						if (self.selectedIds[0].includes(item.id.toString())) {
							let groupId = self.filters[a].id;
							self.selected[groupId].push(item)
						};
					});
				}
			},
			loadMore: function () {
				if ((this.cap + increment) < this.filteredResults.length) {
					this.cap = this.cap + increment;
				} else {
					this.cap = validateCap(this.filteredResults.length);
				}
			},
			gotoUrl: function (url) {
				const noTextSelected = !window.getSelection().toString();
				(noTextSelected && url !== "") ? location.href = url : false;
			},
			getFocus: function (item, id) {
				//  when the title link has focus, give the whole card a focused look
				// the whole card is the second parent of the direct parent of the link
				const direcParent = document.getElementById(id).parentElement;
				direcParent.parentElement.classList.add('focus');
			},
			getBlur: function (item, id) {
				//  when the title link is not focused,remove the focus class
				// the whole card is the second parent of the direct parent of the link
				const direcParent = document.getElementById(id).parentElement;
				direcParent.parentElement.classList.remove('focus');
			},
		},
		computed: {
			/**
			 * filter the raw results down by the filters
			 * @return {array} filtered results
			 */
			filteredResults: function () {
				var filteredResults = [];

				if (this.allResults && this.allResults.length) {

					// for each resource
					for (var i = 0; i < this.allResults.length; i++) {
						var item = this.allResults[i];

						let allFiltersHaveAMatch = false;
						let filtersMatching = [];
						let selectedFilters = Object.keys(this.selected);
						let hasSelectedFilters = false;
						for (let index = 0; index < selectedFilters.length; index++) {
							const filter = this.selected[selectedFilters[index]];
							if (filter.length) {
								filtersMatching[filter[0].section] = false;
								hasSelectedFilters = true
							}
						}

						// Within a filter - OR
						// Between filters - AND
						if (hasSelectedFilters) {

							allFiltersHaveAMatch = true;
							// for each active filter
							for (var a = 0; a < selectedFilters.length; a++) {

								var filter = this.selected[selectedFilters[a]];

								// for each topic/type/category/audience etc selected in the filter
								for (var k in filter) {
									let withinFilterMatch = false;
									if (filter.hasOwnProperty(k)) {
										let section = filter[k].section;
										let itemIds = Object.keys(item[section]);

										for (let index = 0; index < itemIds.length; index++) {
											const itemId = itemIds[index];
											if (parseInt(itemId) === parseInt(filter[k].id)) {
												withinFilterMatch = true;
												filtersMatching[section] = true;
											}
										}
									}
								}
							}

							// Make sure all the DIFFERENT filters are matching (the AND part)
							for (var x in filtersMatching) {
								if (filtersMatching[x] == false) {
									allFiltersHaveAMatch = false;
								}
							}

						} else {
							allFiltersHaveAMatch = true;
						}

						// In this example, we filter the results by keyword - searching through title and summary
						let keywordMatch = false;
						let trimmedLowerKeyword = this.keyword.trim().toLowerCase();
						if (
							!trimmedLowerKeyword
							|| trimmedLowerKeyword == ""
							|| item.title.toLowerCase().indexOf(trimmedLowerKeyword) !== -1
							|| item.summary.toLowerCase().indexOf(trimmedLowerKeyword) !== -1
							// || findInTags1(trimmedLowerKeyword, item.tags)
						) {
							keywordMatch = true;
						}

						if (keywordMatch && allFiltersHaveAMatch) {
							filteredResults.push(item);
						}

					}

				}
				return filteredResults;

			},

			/**
			 * Sort the filtered results by certain criteria. This function may not be needed based on the requirements.
			 * @return {array} sorted filtered results
			 */
			sortedFilteredResults: function () {
				if (this.filteredResults) {
					var cloneResults = this.filteredResults.slice(0);

					cloneResults.sort(function (p1, p2) {
						if (p1.title < p2.title) {
							return -1;
						}
						if (p1.title > p2.title) {
							return 1;
						}
						return 0;
					});
					return cloneResults;
				}
				return [];
			},

			/**
			 *  - 2 that are featured otherwise any 2 (always have 2 featured if there are 2)
			 * DEPRECATED AS AMENDED TO HAVE NO FEATURED AREA
			**/
			// featuredResults: function () {

			// 	// if two or less total results then just return all
			// 	if (this.cappedSortedFilteredResults.length < 3) {
			// 		return this.cappedSortedFilteredResults;
			// 	}

			// 	// Find featured results
			// 	const featuredResources = this.cappedSortedFilteredResults.filter(function (item) {
			// 		return item.featured == true;
			// 	});
				
			// 	// if only one featured result found, grab the next one as well so we have two
			// 	if(featuredResources.length === 1) {
			// 		let anotherResource = this.cappedSortedFilteredResults.find(function (item) {
			// 			return item.featured == false;
			// 		});
			// 		if (anotherResource) {
			// 			featuredResources.push(anotherResource)
			// 		}
			// 	}

			// 	// If none just grab the first two
			// 	if (featuredResources.length === 0) {
			// 		return this.cappedSortedFilteredResults.slice(0, 2)
			// 	}

			// 	return featuredResources.slice(0, 2);
			// },

			// notFeaturedResults: function() {

			// 	// If we have 2 or less then all will show in the featured section (whether has featured property or not)
			// 	if (this.cappedSortedFilteredResults.length == 2) {
			// 		return null;
			// 	}

			// 	const cappedFeaturedResourcesIds = this.featuredResults.map( function (item) {
			// 		return item.id;
			// 	});

			// 	const notFeaturedResults = this.cappedSortedFilteredResults.filter( function (item) {
			// 		return !cappedFeaturedResourcesIds.includes(item.id);
			// 	});

			// 	return notFeaturedResults;
			// },

			/**
			 * Cap the results (the logics to work with the "Load more" button). You probably don't need to update this function
			 * @return {array} capped sorted filtered results
			 */
			cappedSortedFilteredResults: function () {
				if (this.sortedFilteredResults.length > 0) {
					return this.sortedFilteredResults.slice(0, this.cap); // if cap is larger than the length, will return the full  array instead of an error
				}
				return [];
			},

			showingCount: function () {
				if (this.cap < this.filteredResults.length) {
					return this.cap;
				} 
				return this.filteredResults.length;
			},

			showLoadMore: function () {
				if (this.filteredResults.length > 0) {
					if (this.cap < this.filteredResults.length) {
						return true;
					}
				}
				return false;
			},
		},
		watch: {
			selected: {
				handler(selected) {
					updateUrlParams(this.cap, selected, this.keyword);
				},
				deep: true
			},
			keyword: function (newVal) {
				updateUrlParams(this.cap, this.selected, newVal);
			},
			cap: function (newVal) {
				updateUrlParams(newVal, this.selected, this.keyword);
			}
		},
		mounted() {
			axios
				.get('/actions/yump-module/default/get-resources', {
					params: {
						entryId: $('#resource-list-vue').data('entry-id')
					}
				})
				.then(response => {
					console.log(response.data);
					this.allResults = response.data.items;
					this.filters = response.data.filters;
					this.showTopTags = response.data.showTopTags;
					// Dynamically set reactive selected filters
					for (let i = 0; i < ObjectLength(this.filters); i++) {
						const filter = this.filters[i];
						Vue.set(resourcesVue.selected, filter.id, []);
					}

					this.initSelectedIds()

				})
				.catch(error => {
					let errMessage = "We're sorry, we're not able to retrieve the data at this moment, please check your internet and try refreshing your browser.";
					// console.log(error);
					// if(error.response.data.error) {
					// 	errMessage = error.response.data.error;
					// }
					this.initErrored = true;
					this.message.error = errMessage;
				})
				.finally(() => {
					this.initialising = false
				})
		},
	});
});

const groupBy = key => array =>
array.reduce((objectsByKeyValue, obj) => {
	const value = obj[key];
	objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
	return objectsByKeyValue;
}, {});

function updateUrlParams(cap, selected, keyword) {

	if (!$('html').hasClass('ieLessThan10')) { // otherwise we will hit js error and break other scripts

		let paramString = '';
		let selectedUrlStr = false;

		for (const key in selected) {
			const selectedObj = selected[key];
			if (selectedObj.length > 0) {
				selectedUrlStr = true
				selectedObj.map(function (sel) {
					paramString = paramString + sel.id + ',';
				});
			}
		}

		if (selectedUrlStr) {
			paramString = "selectedIds=" + paramString
		}

		// http://localhost:3000/resources?selectedIds=11843,

		paramString = paramString + objToString({
			cap: cap && cap > initShowingItems ? cap : null,
			keyword: keyword && keyword ? keyword : null,
		});

		var newUrl = window.location.origin + window.location.pathname + '?' + paramString;
		history.replaceState(null, null, newUrl); 

	}
}

function objToString(obj) {
	var str = '';
	for (var p in obj) {
		if (obj.hasOwnProperty(p) && obj[p]) {
			str += p + '=' + encodeURIComponent(obj[p]) + '&'; // need to encode the value, cuz users might put in white spaces, etc
		}
	}
	if (str.substr(str.length - 1) == '&') {
		str = str.substr(0, str.length - 1);
	}
	return str;
}

function findInTags(keyword, tags) {
	if (tags) {
		for (var i = 0; i < tags.length; i++) {
			const tag = tags[i];
			if (tag.toLowerCase() == keyword) {
				return true;
			}
		}
	}

	return false;
}

function findInTags1(keyword, tags) {
	if (!tags) {
		return false;
	}
	return tags.find(function (e) { return e.toLowerCase().indexOf(keyword) !== -1 })
}

// Determine if there is a common element within two arrays
function findCommonElement(array1, array2) {

	// Loop for array1
	for (let i = 0; i < array1.length; i++) {

		// Loop for array2
		for (let j = 0; j < array2.length; j++) {

			// Compare the element of each and
			// every element from both of the
			// arrays
			if (array1[i] === array2[j]) {

				// Return if common element found
				return true;
			}
		}
	}

	// Return if no common element exist
	return false;
}

// Copy an entire object/array 
const deepCopy = (arr) => {
	let copy = [];
	arr.forEach(elem => {
		if (Array.isArray(elem)) {
			copy.push(deepCopy(elem))
		} else {
			if (typeof elem === 'object') {
				copy.push(deepCopyObject(elem))
			} else {
				copy.push(elem)
			}
		}
	})
	return copy;
}

// Helper function to deal with Objects
const deepCopyObject = (obj) => {
	let tempObj = {};
	for (let [key, value] of Object.entries(obj)) {
		if (Array.isArray(value)) {
			tempObj[key] = deepCopy(value);
		} else {
			if (typeof value === 'object') {
				tempObj[key] = deepCopyObject(value);
			} else {
				tempObj[key] = value
			}
		}
	}
	return tempObj;
}

function isInt(value) {
	return !isNaN(value) && (function (x) { return (x | 0) === x; })(parseFloat(value))
}

function ObjectLength(object) {
	var length = 0;
	for (var key in object) {
		if (object.hasOwnProperty(key)) {
			++length;
		}
	}
	return length;
};

