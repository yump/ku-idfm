Vue.component("multiselect", window.VueMultiselect.default);
const initShowingItems = 9;
const increment = 9;
initUrlParams();


function initUrlParams() {
    window.urlParameters = {
        cap: null,
        keyword: "",
        types: null,
    };

    let url = null;
    try {
        const url_string = window.location.href;
        url = new URL(url_string);
    } catch (err) {
        // console.log(err);
    }

    if (url) {
        window.urlParameters.cap = url.searchParams.get("cap")
            ? url.searchParams.get("cap")
            : null;
        window.urlParameters.keyword = url.searchParams.get("keyword") ? url.searchParams.get("keyword").trim() : "";
        const equipmentTypes = url.searchParams.get("types") ? url.searchParams.get("types") : null;
        // console.log(equipmentTypes)
        if (equipmentTypes) {
            window.urlParameters.types = equipmentTypes.split(',');
        } else {
            window.urlParameters.types = null;
        }
    }
}

// make 'cap' always (initShowingItems + increment * x), to avoid showing weird number of items with load more button
function validateCap(rawNumber) {
    // my weird math stuff...
    const m = (rawNumber - initShowingItems) / increment;
    const x = isInt(m) ? m : Math.floor(m + 1);
    const validatedVal = increment * x + initShowingItems;
    // console.log('m: ' + m, 'x: ' + x, "val: " + validatedVal);
    return validatedVal;
}

function isInt(value) {
    return (
        !isNaN(value) &&
        (function (x) {
            return (x | 0) === x;
        })(parseFloat(value))
    );
}
function objToString(obj) {
    let str = "";
    for (let p in obj) {
        if (obj.hasOwnProperty(p) && obj[p]) {
            str += p + "=" + encodeURIComponent(obj[p]) + "&"; // need to encode the value, cuz users might put in white spaces, etc
        }
    }
    if (str.substr(str.length - 1) == "&") {
        str = str.substr(0, str.length - 1);
    }
    return str;
}
window.equipmentLibraryVue = new Vue({
    el: "#equipment-library-vue",
    delimiters: ["{*", "*}"],
    data: {
        allResults: null,
        initErrored: false,
        message: {
            info: null,
            notice: null,
            error: null,
        },
        initialising: true,
        keyword: "",
        equipmentTypes: [],
        cap:
            window.urlParameters.cap &&
                validateCap(window.urlParameters.cap)
                ? validateCap(window.urlParameters.cap)
                : initShowingItems,
    },
    methods: {
        gotoUrl: function (url) {
            const noTextSelected = !window.getSelection().toString();
            (noTextSelected && url !== "") ? location.href = url : false;
        },
        getFocus: function (item, id) {
            //  when the title link has focus, give the whole card a focused look
            // the whole card is the second parent of the direct parent of the link
            const direcParent = document.getElementById(id).parentElement;
            direcParent.parentElement.classList.add('focus');
        },
        getBlur: function (item, id) {
            //  when the title link is not focused,remove the focus class
            // the whole card is the second parent of the direct parent of the link
            const direcParent = document.getElementById(id).parentElement;
            direcParent.parentElement.classList.remove('focus');
        },
        loadMore: function () {
            if (this.cap + increment < this.filteredResults.length) {
                this.cap = this.cap + increment;
            } else {
                this.cap = validateCap(this.filteredResults.length);
            }
        },
        initialiseFilters: function () {
            // initialise keyword
            if (urlParameters.keyword && urlParameters.keyword != null) {
                window.equipmentLibraryVue.keyword = urlParameters.keyword.trim();
            }
            // check the type on url and update the multiselect options accordingly
            if (window.urlParameters.types) {
                const types = window.urlParameters.types;
                const rawTypes = window.equipmentLibraryVue.allResults.equipmentTypes;
                console.log({ types, rawTypes })
                types.forEach((paramerterType) => {
                    if (rawTypes.length > 0) {
                        rawTypes.forEach((rawType) => {
                            console.log({ paramerterType })
                            console.log(rawType.value)
                            if (paramerterType == rawType.value) {
                                window.equipmentLibraryVue.equipmentTypes.push(rawType);
                            }
                        });
                    }
                });
            }
        },
    },
    computed: {
        /**
         * filter the raw results down by the filters
         * @return {array} filtered results
         */
        filteredResults: function () {
            let filteredResults = [];
            if (this.allResults && this.allResults.entries.length) {
                this.allResults.entries.forEach((entry) => {
                    // keyword filter
                    let trimmedLowerKeyword = this.keyword.trim().toLowerCase();
                    let keywordMatch = false;
                    if (trimmedLowerKeyword != "") {
                        keywordMatch = false;
                        if (
                            !trimmedLowerKeyword
                            || trimmedLowerKeyword == ""
                            || entry.title.toLowerCase().indexOf(trimmedLowerKeyword) !== -1
                            || entry.summary.toLowerCase().indexOf(trimmedLowerKeyword) !== -1
                            || entry.available.toLowerCase().indexOf(trimmedLowerKeyword) !== -1
                            || findInTags(trimmedLowerKeyword, entry.tags) == true
                            || findInTypes(trimmedLowerKeyword, entry.equipmentType) == true
                        ) {
                            keywordMatch = true;
                        }
                        if (!keywordMatch) {
                            return false;
                        }
                    }
                    // types filter
                    if (this.equipmentTypes.length > 0) {
                        let typeMatch = false;
                        const entryTypes = Object.values(entry.equipmentType);
                        const filterTypes = this.equipmentTypes;
                        filterTypes.forEach(filterType => {
                            entryTypes.forEach(entryType => {
                                if (entryType.value == filterType.value) {
                                    typeMatch = true;
                                }
                            });

                        });
                        if (!typeMatch) {
                            return false;
                        }
                    }
                    filteredResults.push(entry);
                });
                return filteredResults;
            }
        },
        /**
         * Cap the results (the logics to work with the "Load more" button). You probably don't need to update this function
         * @return {array} capped sorted filtered results
         */
        cappedSortedFilteredResults: function () {
            if (this.filteredResults.length > 0) {
                return this.filteredResults.slice(0, this.cap); // if cap is larger than the length, will return the full filteredSpecialists array instead of an error
            }
            return [];
        },
        showingCount: function () {
            if (this.cap < this.filteredResults.length) {
                return this.cap;
            }
            return this.filteredResults.length;
        },
        showLoadMore: function () {
            if (this.filteredResults.length > 0) {
                if (this.cap < this.filteredResults.length) {
                    return true;
                }
            }
            return false;
        },
    },
    watch: {
        keyword: {
            handler: function (newVal) {
                updateUrlParams(newVal, this.equipmentTypes, this.cap);
            },
            deep: true,
        },
        equipmentTypes: {
            handler: function (newVal) {
                updateUrlParams(this.keyword, newVal, this.cap);
            },
            deep: true,
        },
        cap: function (newVal) {
            updateUrlParams(this.keyword, this.equipmentTypes, newVal);
        },
    },
    mounted() {
        axios
            .get(
                "/actions/yump-module/default/get-specialist-equipment-items"
            )
            .then((response) => {
                if (response.data) {
                    window.equipmentLibraryVue.allResults = response.data;
                    this.initialiseFilters();
                }

            })
            .catch((error) => {
                let errMessage =
                    "We're sorry, we're not able to retrieve the data at this moment, please check your internet and try refreshing your browser.";
                if (error.response.data.error) {
                    errMessage = error.response.data.error;
                }
                this.initErrored = true;
                this.message.error = errMessage;
            })
            .finally(() => (this.initialising = false));
    },
});

function updateUrlParams(keyword, equipmentTypes, cap) {
    if (!$("html").hasClass("ieLessThan10")) {
        // otherwise we will hit js error and break other scripts
        let types = null;
        // console.log(types)
        const filterTypes = equipmentTypes;
        if (filterTypes) {
            for (let [index, type] of filterTypes.entries()) {
                let add = "";
                if (index > 0) {
                    add = ",";
                    types += add + type.value;
                } else {
                    types += type.value;
                }
            }
        }
        if (types == null) {
            equipmentTypes = null;
        }
        const paramString = objToString({
            keyword: keyword ? keyword : null,
            types: types,
            cap: cap && cap > initShowingItems ? cap : null,

        });

        const newUrl =
            window.location.origin +
            window.location.pathname +
            "?" +
            paramString;
        history.replaceState(null, null, newUrl); // Doesn't work for IE 10 and less, might need to change this into window.location.hash
    }
}
function findInTags(keyword, tags) {
    if (tags) {
        // check if the keyword matches any tag as we type
        const match = (tag) => (tag.toLowerCase() == keyword) ? true : false;
        // if any tag mathces the keyword, return true, if not all, return false
        if (tags.some(match)) {
            return true
        }

    }
    return false
}
function findInTypes(keyword, types) {
    if (types) {
        // check if the keyword matches any type as we type
        const match = (type) => (type.title.toLowerCase() == keyword) ? true : false;
        // if any type mathces the keyword, return true, if not all, return false
        if (types.some(match)) {
            return true
        }

        return false
    }
}