if (!window.Yump) {
	window.Yump = {};
}

// detect IE and add 'ie' class to body
Yump.isIE = "ActiveXObject" in window; //window.ActiveXObject !== undefined;
if (Yump.isIE) {
	document.body.classList.add("ie");
}


// script for all caards to make image of the card clickable -  We make sure all content is loaded first
document.addEventListener("DOMContentLoaded", function () {
	// This is extra check to make sure there are elements to select
	if ("querySelector" in document) {
		// Select all the cards using class - it could change based on our needs or can be a variable
		const cards = document.querySelectorAll(".card");
		// When using const , always make sure if it exists otherwise we will have console errors, it won't break anything but avoids errors
		if (cards) {
			cards.forEach((card) => {
				// find all anchor tags within card and stop them from propagation - this means if we click on a "a" tag, it will not trigger the    card clock and the other way around
				// const clickableElements = Array.from(card.querySelectorAll("a"));
				// clickableElements.forEach((ele) =>
				// 	ele.addEventListener("click", (e) => e.stopPropagation())
				// );
				// find the card link, if not empty - I use the data-set hrer and not the anchor link's url as it is better practice for this porpose
				const link = card.dataset.link;
				// will take care of click event for image here (the figure tag)
				const image = card.querySelector('figure');
				image.addEventListener("click", (event) => {
					// check if any text gets selected , for this purpose within the card component
					// const noTextSelected = !window.getSelection().toString();
					// If no text is selected and the link above is not empty then click event will fire, otherwise nothing - this is good if card has no link at all and it is just a static card
					(link !== "") ? location.href = link : false;
				})
				// this part is to give the card visible focus since the card itself is not a focusable element. 
				// we get the anchor tag of card ( that we specified with card's link) and use it 
				const directLink = card.querySelector('.card__title-link');
				// use focus events to target the card element. In this example the card is the seconda top parent which is in all cards we have pretty much. In CSS we cannot use parent selector so we have to use Javascript here to give it focus
				directLink.addEventListener("focus", (event) => {
					const direcParent = event.target.parentElement;
					// the whole card is the second parent of the direct parent of the link
					//  when the title link has focus, give the whole  card a focused look - - will add the focus class in our css
					direcParent.parentElement.classList.add('focus');
				})
				// we should use blur to remove the focus
				directLink.addEventListener("blur", (event) => {
					const direcParent = event.target.parentElement;
					// the whole card is the second parent of the direct parent of the link
					//  when the title link has focus, give the whole  card a focused look 
					direcParent.parentElement.classList.remove('focus');
				})
			});
		}
	}
});
