import {scrollToElement} from "../services/utility";

// <button class='button button--primary' data-action='scrollToElement' data-target='#main'>scroll to main</button>

+$(function () {
    $("[data-action~='scrollToElement']").each(function () {
        $(this).click(function () {
            const target = $(this).attr("data-target");
            scrollToElement(target);
        });
    });
});
