// When the user scrolls the page, execute myFunction
window.onscroll = function () { myFunction() };

// Get the header
var header = document.getElementById("site-header");
// var utilityNav = 

// Get the offset position of the navbar
var sticky = header.offsetTop + 100;
var slideDown = header.offsetTop + 400;
// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
	if (window.pageYOffset > sticky) {
		header.classList.add("header--sticky");
	} else {
		header.classList.remove("header--sticky");
	}
	if (window.pageYOffset > slideDown) {
		header.classList.add("header--slide-down");
	} else {
		header.classList.remove("header--slide-down");
	}
}