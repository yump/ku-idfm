import { alertSpacer, initAlert, closeAlert } from '../services/alert';

$('[data-alert]').each(function () {
    initAlert($(this).attr('data-alert'));
});

$('[data-alert-close]').on('click', function (e) {
    // Do not perform default action when button is clicked
    e.preventDefault();
    closeAlert($(this).attr('data-alert-close'));
    alertSpacer.css({display: 'none'}).hide();
});
