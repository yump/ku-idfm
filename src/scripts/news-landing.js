const initShowingItems = 6;
const increment = 3;

initUrlParams();

function initUrlParams() {
	window.urlParameters = {
		cap: null,
		types: [],
		keyword: "",
	};

	var url = null;
	try {
		var url_string = window.location.href;
		url = new URL(url_string);
	}
	catch(err) {
		// console.log(err);
	}

	if(url) {
		window.urlParameters.cap = url.searchParams.get("cap") ? url.searchParams.get("cap") : null;
		window.urlParameters.keyword = url.searchParams.get("keyword") ? url.searchParams.get("keyword") : "";

		const rawTypes = url.searchParams.get("types") ? url.searchParams.get("types") : null;
		if(rawTypes) {
			window.urlParameters.types = urlParameters.type.split(',');
		} else {
			window.urlParameters.types = null;
		}
		
	}

}

// make 'cap' always (initShowingItems + increment * x), to avoid showing weird number of items with load more button
function validateCap(rawNumber) {
  // my weird math stuff...
  var m = (rawNumber - initShowingItems) / increment;
  var x = isInt(m) ? m : Math.floor(m + 1);
  var validatedVal = increment * x + initShowingItems;
  // console.log(validatedVal);
  return validatedVal;
}

function isInt(value) {
  return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
}

$(function() {
	let newsVue = new Vue({
		el: '#news-list-vue',
		delimiters: ['{*', '*}'],
		data: {
			allResults: null,
			initErrored: false,
			message: {
				info: null,
				notice: null,
				error: null,
			},
			initialising: true,
			filters: {
				keyword: window.urlParameters.keyword,
				types: window.urlParameters.types,
			},

			cap: window.urlParameters.cap && validateCap(window.urlParameters.cap) ? validateCap(window.urlParameters.cap) : initShowingItems,
	  	},
	  	methods: {
	  		loadMore: function() {
				if((this.cap + increment) < this.allResults.length) {
					this.cap = this.cap + increment;
				} else {
					this.cap = validateCap(this.allResults.length);
				}
			},
			gotoUrl: function (url) {
				const noTextSelected = !window.getSelection().toString();
				(noTextSelected && url !== "") ? location.href = url : false;
			},
			getFocus: function (item, id) {
				//  when the title link has focus, give the whole card a focused look
				// the whole card is the second parent of the direct parent of the link
				const direcParent = document.getElementById(id).parentElement;
				direcParent.parentElement.classList.add('focus');
			},
			getBlur: function (item, id) {
				//  when the title link is not focused,remove the focus class
				// the whole card is the second parent of the direct parent of the link
				const direcParent = document.getElementById(id).parentElement;
				direcParent.parentElement.classList.remove('focus');
			},
	  	},
	  	computed: {
	  		/**
	  		 * filter the raw results down by the filters
	  		 * @return {array} filtered results
	  		 */
	  		filteredResults: function() {
	  			var filteredResults = [];
	  			if(this.allResults && this.allResults.length) {
	  				for (var i = 0; i < this.allResults.length; i++) {
	  					var item = this.allResults[i];

	  					// TODO: build your filter logics here
	  					// if(this.filters[item.filterKey]) {
	  					// 	filteredResults.push(item);
	  					// }
	  					
	  				}
	  			}
	  			return filteredResults;
	  		},
	  		/**
	  		 * Sort the filtered results by certain criteria. This function may not always apply based on the requirements
	  		 * @return {array} sorted filtered results
	  		 */
	  		// sortedFilteredResults: function() {
	  		// 	if(this.filteredResults) {
	  		// 		var cloneResults = this.filteredResults.slice(0);
	  		// 		cloneResults.sort(function(p1, p2) {
		  	// 			if(p1.score < p2.score) {
		  	// 				return 1;
		  	// 			}
		  	// 			if(p1.score > p2.score) {
		  	// 				return -1;
		  	// 			}
		  	// 			return 0;
		  	// 		});
		  	// 		return cloneResults;
	  		// 	}
	  		// 	return [];
	  		// },
	  		/**
	  		 * Cap the results (the logics to work with the "Load more" button)
	  		 * @return {array} capped sorted filtered results
	  		 */
	  		cappedSortedFilteredResults: function() {
	  			if(this.allResults.length > 0) {
	  				return this.allResults.slice(0, this.cap); // if cap is larger than the length, will return the full filteredSpecialists array instead of an error
	  			}
	  			return [];
	  		},
	  		showLoadMore: function() {
				if(this.cappedSortedFilteredResults.length > 0) {
					if (this.cappedSortedFilteredResults.length < this.allResults.length) {
						return true;
					}
				}
				return false;
			}, 
	  	},
	  	watch: {
	  		filters: {
	  			handler: function(newVal) {
	  				updateUrlParams(newVal, this.cap);
	  			},
	  			deep: true,
	  		},
	  		cap: function(newVal) {
	  			updateUrlParams(this.filters, newVal);
	  		}
	  	},
	  	mounted() {
        	axios
            .get('/actions/yump-module/default/get-news-items')
            .then(response => {
                this.allResults = response.data;

                console.log(response.data);

            })
            .catch(error => {
                let errMessage = "We're sorry, we're not able to retrieve the data at this moment, please check your internet and try refreshing your browser.";
                if(error.response.data.error) {
                	errMessage = error.response.data.error;
                }
                this.initErrored = true;
                this.message.error = errMessage;
            })
            .finally(() => this.initialising = false)
    	},
	});
});

function updateUrlParams(filters, cap) {
	if(!$('html').hasClass('ieLessThan10')) { // otherwise we will hit js error and break other scripts

    var paramString = objToString({
    	showing: cap && cap > initShowingItems ? cap : null,
    });

	var newUrl = window.location.origin + window.location.pathname + '?' + paramString;
	history.replaceState(null, null, newUrl); // Doesn't work for IE 10 and less, might need to change this into window.location.hash
	}
}

function objToString (obj) {
    var str = '';
    for (var p in obj) {
        if (obj.hasOwnProperty(p) && obj[p]) {
            str += p + '=' + encodeURIComponent(obj[p]) + '&'; // need to encode the value, cuz users might put in white spaces, etc
        }
    }
    if (str.substr(str.length - 1) == '&') {
      str = str.substr(0, str.length - 1);
    }
    return str;
}