const alertSpacer = $('.header__spacer--alert');
const alertContainer = $('.site-wide-alert');
/**
 * If you have a sticky header make sure to update this one to true
 *
 * This will automatically create a spacer based on your top alerts height
 * You may also change this variable into window.stickyHeader so you can modify it in other scripts
 *
 * @type {boolean}
 */
const stickyHeader = true;

const initAlert = (alertID) => {
    // Check if alert has been closed
    if (getCookie('alert-' + alertID) !== 'closed') {
        const alertBlock = jQuery('[data-alert="' + alertID + '"]');

        alertBlock.css({display: 'block'}).show();
        alertSpacer.css({display: 'block'}).show();

        updateSpacerHeight();

        //update height when resize
        window.addEventListener('resize', function () {
            updateSpacerHeight();
        });
    }
};

const updateSpacerHeight = () => {
    if(stickyHeader){
        const alertHeight = alertContainer.outerHeight();
        window.alertHeight = alertHeight;
        alertSpacer.css({
            'padding-top': alertHeight
        });
    }
};

const closeAlert = (alertID) => {
    setCookie('alert-' + alertID, 'closed', 2);
    $('[data-alert="' + alertID + '"]').slideUp();
};

const setCookie = (key, value, expiry) => {
    const expires = new Date();
    expires.setTime(expires.getTime() + expiry * 24 * 60 * 60 * 1000);
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
};

const getCookie = (key) => {
    const keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null
};

const eraseCookie = (key) => {
    const keyValue = getCookie(key);
    setCookie(key, keyValue, '-1');
};

export {
    alertContainer,
    alertSpacer,
    updateSpacerHeight,
    initAlert,
    closeAlert,
}
