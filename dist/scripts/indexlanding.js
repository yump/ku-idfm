/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/scripts/yump/indexlanding.js":
/*!******************************************!*\
  !*** ./src/scripts/yump/indexlanding.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nnew Vue({\n    el: \".vue\",\n    data: function data() {\n        return {\n            eventsIndex: [{ name: 'Derek', description: 'Some comment' }, {\n                name: 'Joe',\n                description: 'Some comment'\n            }, { name: 'Mike', description: 'Some comment' }, { name: 'Ron', description: 'Some comment' }, {\n                name: 'Dii',\n                description: 'Some comment'\n            }, { name: 'Lonnie', description: 'Some comment' }, {\n                name: 'Paul',\n                description: 'Some comment'\n            }, { name: 'Mike', description: 'Some comment' }, { name: 'Jody', description: 'Some comment' }, {\n                name: 'Ryn',\n                description: 'Some comment'\n            }, { name: 'Jord', description: 'Some comment' }, { name: 'Milly', description: 'Some comment' }, {\n                name: 'Judy',\n                description: 'Some comment'\n            }, { name: 'Vanilly', description: 'Some comment' }, {\n                name: 'Nolan',\n                description: 'Some comment'\n            }, { name: 'Pino', description: 'Some comment' }, { name: 'Ryne', description: 'Some comment' }, {\n                name: 'Scott',\n                description: 'Some comment'\n            }, { name: 'Son', description: 'Some comment' }, { name: 'Bann', description: 'Some comment' }],\n            commentsToShow: 2\n        };\n    }\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyaXB0cy95dW1wL2luZGV4bGFuZGluZy5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3NyYy9zY3JpcHRzL3l1bXAvaW5kZXhsYW5kaW5nLmpzPzdlNzgiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5uZXcgVnVlKHtcbiAgICBlbDogXCIudnVlXCIsXG4gICAgZGF0YTogZnVuY3Rpb24gZGF0YSgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGV2ZW50c0luZGV4OiBbeyBuYW1lOiAnRGVyZWsnLCBkZXNjcmlwdGlvbjogJ1NvbWUgY29tbWVudCcgfSwge1xuICAgICAgICAgICAgICAgIG5hbWU6ICdKb2UnLFxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnU29tZSBjb21tZW50J1xuICAgICAgICAgICAgfSwgeyBuYW1lOiAnTWlrZScsIGRlc2NyaXB0aW9uOiAnU29tZSBjb21tZW50JyB9LCB7IG5hbWU6ICdSb24nLCBkZXNjcmlwdGlvbjogJ1NvbWUgY29tbWVudCcgfSwge1xuICAgICAgICAgICAgICAgIG5hbWU6ICdEaWknLFxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnU29tZSBjb21tZW50J1xuICAgICAgICAgICAgfSwgeyBuYW1lOiAnTG9ubmllJywgZGVzY3JpcHRpb246ICdTb21lIGNvbW1lbnQnIH0sIHtcbiAgICAgICAgICAgICAgICBuYW1lOiAnUGF1bCcsXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICdTb21lIGNvbW1lbnQnXG4gICAgICAgICAgICB9LCB7IG5hbWU6ICdNaWtlJywgZGVzY3JpcHRpb246ICdTb21lIGNvbW1lbnQnIH0sIHsgbmFtZTogJ0pvZHknLCBkZXNjcmlwdGlvbjogJ1NvbWUgY29tbWVudCcgfSwge1xuICAgICAgICAgICAgICAgIG5hbWU6ICdSeW4nLFxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnU29tZSBjb21tZW50J1xuICAgICAgICAgICAgfSwgeyBuYW1lOiAnSm9yZCcsIGRlc2NyaXB0aW9uOiAnU29tZSBjb21tZW50JyB9LCB7IG5hbWU6ICdNaWxseScsIGRlc2NyaXB0aW9uOiAnU29tZSBjb21tZW50JyB9LCB7XG4gICAgICAgICAgICAgICAgbmFtZTogJ0p1ZHknLFxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnU29tZSBjb21tZW50J1xuICAgICAgICAgICAgfSwgeyBuYW1lOiAnVmFuaWxseScsIGRlc2NyaXB0aW9uOiAnU29tZSBjb21tZW50JyB9LCB7XG4gICAgICAgICAgICAgICAgbmFtZTogJ05vbGFuJyxcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogJ1NvbWUgY29tbWVudCdcbiAgICAgICAgICAgIH0sIHsgbmFtZTogJ1Bpbm8nLCBkZXNjcmlwdGlvbjogJ1NvbWUgY29tbWVudCcgfSwgeyBuYW1lOiAnUnluZScsIGRlc2NyaXB0aW9uOiAnU29tZSBjb21tZW50JyB9LCB7XG4gICAgICAgICAgICAgICAgbmFtZTogJ1Njb3R0JyxcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogJ1NvbWUgY29tbWVudCdcbiAgICAgICAgICAgIH0sIHsgbmFtZTogJ1NvbicsIGRlc2NyaXB0aW9uOiAnU29tZSBjb21tZW50JyB9LCB7IG5hbWU6ICdCYW5uJywgZGVzY3JpcHRpb246ICdTb21lIGNvbW1lbnQnIH1dLFxuICAgICAgICAgICAgY29tbWVudHNUb1Nob3c6IDJcbiAgICAgICAgfTtcbiAgICB9XG59KTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/scripts/yump/indexlanding.js\n");

/***/ }),

/***/ 2:
/*!************************************************!*\
  !*** multi ./src/scripts/yump/indexlanding.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\projects\yumply\src\scripts\yump\indexlanding.js */"./src/scripts/yump/indexlanding.js");


/***/ })

/******/ });