# Maintenance Reports

## 12 Dec 2024 by Parry Huang

* Craft CMS and plugin update
  - craft 4.7.4 => 4.13.7
  - blitz 4.11.2 => 4.23.9
  - field-manager 3.0.8 => 3.0.9
  - freeform 5.0.12 => 5.8.5
  - imager-x 4.3.1 => 4.5.0
  - neo 4.0.5 => 4.2.23
  - redactor 3.0.4 => 3.1.0
  - retour 4.1.15 => 4.1.20
  - seomatic 4.0.42 => 4.1.8
  - tablemaker 4.0.7 => 4.0.14
* cPanel: error logs , all good
* cPanel: Database auto backup - running fine
* cPanel:
    Disk Usage: 10.63 GB / 140 GB (91%) (Sharing storage with ku.com.au server)
    File Usage: 158,265 / 600,000 (26.38%)
* CMS: content blocks, resources, Special Equipment Library ,SEO functionalities, forms - all good
* Error logs - all good
* Check Google Search Console - all done
* Broken links:
  [Linked staging links, please update the URL to be production.]
  https://idfm.test.yump.com.au/funding-streams/additional-educator<a href>OutboundNoindex
  Linked from: https://idfm.org.au/for-families and https://idfm.org.au/for-services and https://idfm.org.au/funding-streams/immediate-time-limited/changes
  [Linked staging links, please update the URL to be production.]
  https://idfm.test.yump.com.au/funding-streams/immediate-time-limited<a href>OutboundNoindex
  Linked from: https://idfm.org.au/for-families and https://idfm.org.au/for-services
  [Linked staging links, please update the URL to be production.]
  https://idfm.test.yump.com.au/funding-streams/family-day-care-top-up<a href>OutboundNoindex
  Linked from: https://idfm.org.au/for-families and https://idfm.org.au/for-services
  404 Not found
  https://idfm.org.au/funding-streams/innovative-solutions/claim<a href>RedirectedNoindex
  Linked from: https://idfm.org.au/about-the-is-portal
  404 Not found
  https://www.idfm.org.au/WWW_IDFM/media/Media/IDF-Complaints-and-Appeals-Process_2.pdf<a href>RedirectedNoindex
  Linked from: https://idfm.org.au/for-services
  404 Not found
  https://www.ku.com.au/uploads/PDFs/G_KU-Privacy-Policy_V.2.pdf<a href>Outbound
  Linked from: https://idfm.org.au/privacy-policy
  404 Not found
  https://www.idfm.org.au/WWW_IDFM/media/Media/Claiming-IDF-Subsidy-Fact-Sheet-Sept-2021-(003).pdf<a href>RedirectedNoindex
  Linked from: https://idfm.org.au/funding-streams/immediate-time-limited/claim
* Fixes:
  - Fix an issue on rich media have a odd boder bottom on tablet screen width
  - Optimise maintenance process
  - Removed unncessary zip files on server to save resources. (staging database backups, logs, zipped uploads)

## 08 Sep 2022 by Alice Zhan

* Craft CMS and plugin update
  "craftcms/cms": "3.8.14" => "3.9.1",
  "nystudio107/craft-retour": "3.2.10" => "3.2.11",
  "nystudio107/craft-seomatic": "3.4.57" => "3.4.59",
  "putyourlightson/craft-blitz": "3.12.9" => "3.12.10",
  "solspace/craft-freeform": "3.13.29" => "3.13.32",
  "spacecatninja/imager-x": "v3.6.7" => "v3.6.8",
* staging site has feature waiting for approvement and not yet deploy to production. 
* nav bar on tablet looks too crowded, do we need to do any change on it or just leave it (see image attached)?
* cPanel: error logs , all good
* cPanel: Database auto backup - running fine
* cPanel:
  File Usage: 158,265 / 600,000 (26.38%), 
  Physical Memory Usage:179.9 MB / 4 GB (4.39%)
* CMS: content blocks, resources, Special Equipment Library ,SEO functionalities, forms - all good
* Error logs - all good
* Check Google Search Console - all done
* Broken links:
404 not found
https://idfm.org.au/funding-streams/innovative-solutions/iss-bicultural-support/claim
Link From: https://idfm.org.au/about-the-is-portal => [Innovative Solutions]

https://inclusionagencynswact.org.au/about/big-situations/big-situations-child-safety-and-wellbeing
Link From: https://inclusionagencynswact.org.au/about/big-situations/big-situations-family-violence => [If a family violence situation raises child safety and wellbeing concerns] => [click here]

403 forbidden
https://www.education.gov.au/child-care-package/resources/faq-inclusion-support-portal
Link From: https://idfm.org.au/for-services => [Frequently Asked Questions (FAQs)]
Link also from: https://idfm.org.au/resources/faq-inclusion-support-portal => [FAQ: Inclusion Support Portal]


## 26 June 2023 by Alice Zhan

* Updated Craft to 3.8.14
* Updated all plugins to the latest
* getCsrfInput() has been deprecated. Use csrfInput() instead in forgot-password.html.
* Checked Craft log for errors - nothing to be concerned
* Acronis Backup has 404 issue, already sent ticket to ventra IP and waiting for response.
  - Solved on 27th June. All backups are good.
* Testing
	- Test Multi-site CMS Functionalities
	- Test content blocks
	- Test Front end templates
	- Test resources
	- Test Special Equipment Library
	- Test SEO functionalities
	- Test Contact Form
* cPanel error logs:  All good 
* cPanel: 
    File Usage: 152,407 / 600,000 (25.4%), 
    Physical Memory Usage: 239.48 MB / 4 GB (5.85%)
* Check Google Search Console - all done

* Broken links:
  - 404 Not found:
  https://docs.education.gov.au/documents/inclusion-support-program-and-child-care-subsidy-eligibility-requirements
  (From: https://idfm.org.au/for-services, under section content: [Child Care Subsidy Requirements])

  https://www.idfm.org.au/WWW_IDFM/media/Media/IDF-Complaints-and-Appeals-Process_2.pdf
  (From https://idfm.org.au/for-services, errorly binded on the full stop under section content: [Feedback, Complaints and Appeals].)

  https://idfm.org.au/funding-streams/innovative-solutions/claim
  (From: https://idfm.org.au/about-the-is-portal under content [What is the IS Portal used for?] -> [Viewing Inclusion Support funding claims] -> [Innovative Solutions])

  https://www.idfm.org.au/WWW_IDFM/media/Media/Claiming-IDF-Subsidy-Fact-Sheet-Sept-2021-(003).pdf
  (From: https://idfm.org.au/funding-streams/immediate-time-limited/claim, errorly binded on the full stop after the link [Claiming IDF Subsidy Fact Sheet]. )

  https://inclusionagencynswact.org.au/about/big-situations/big-situations-child-safety-and-wellbeing
  (From: https://inclusionagencynswact.org.au/about/big-situations/big-situations-family-violence under section content: [Resources for preparing or responding] -> 'click here' link at the end of this section.)

  https://vimeo.com/323634675/891ab1c06b
  (From: https://inclusionagencynswact.org.au/about/big-situations/big-situations-natural-disasters under section content: [Children's books relating to this big situation] -> [Emerging Minds - Video])

  - 403 Forbidden:
  https://www.education.gov.au/inclusion-support-program-1/resources/overview-strategic-inclusion-plan
  (From: https://idfm.org.au/strategic-inclusion-plan link "ISP User Guide - Section 2 SIP")
  
  - 500 Internal server error:
  https://grief.org.au/ACGB/ACGB/ACGB_Publications/Resources_for_the_Bereaved/Grief_Information_Sheets.aspx?    hkey=19bfe37f-d79f-4e70-85e7-82b94bca248b
  (From: https://inclusionagencynswact.org.au/about/big-situations/big-situations-loss-and-grief under section content [Information for educators] -> [Grief information sheets])

## Maintenance report for 27th Feb 2023

* Updated Craft CMS and plugins to the latest version as below:
 - craft 3.7.61 => 3.7.67
    - blitz 3.12.7 => 3.12.8
    - freeform 3.13.22.1 => 3.13.24
    - neo 2.13.15 => 2.13.16
    - redactor 2.10.10 => 2.10.11
    - retour 3.2.7 => 3.2.8
    - seomatic 3.4.45 => 3.4.49
* cPanel: error logs , all good
* cPanel: Database auto backup - running fine
* cPanel: 
  File Usage: 144,728 / 600,000 (24.12%), 
  Physical Memory Usage: 833.52 MB / 4 GB (20.35%)
* CMS: content blocks, resources, Special Equipment Library ,SEO functionalities, forms - all good
* Error logs - all good
* Check Google Search Console - all done
* Broken links:
403 Forbidden	
https://www.education.gov.au/inclusion-support-program-1/resources/overview-strategic-inclusion-plan
Linked from: https://idfm.org.au/strategic-inclusion-plan
Host not found	
https://www.dese.gov.au/inclusion-support-program-1/resources/isp-flow-chart-families
Linked from: https://idfm.org.au/for-families
Host not found	
https://www.dese.gov.au/child-care-package/child-care-safety-net/inclusion-support-program/inclusion-support-portal
Linked from: https://idfm.org.au/for-families
Host not found	
https://docs.education.gov.au/documents/inclusion-support-program-and-child-care-subsidy-eligibility-requirements
Linked from: https://idfm.org.au/for-services
404 Not found	
https://www.idfm.org.au/WWW_IDFM/media/Media/IDF-Complaints-and-Appeals-Process_2.pdf
Linked from: https://idfm.org.au/for-services
404 Not found	
https://www.idfm.org.au/WWW_IDFM/media/Media/Claiming-IDF-Subsidy-Fact-Sheet-Sept-2021-(003).pdf
Linked from: https://idfm.org.au/funding-streams/immediate-time-limited/claim
Host not found	
https://www.dese.gov.au/inclusion-support-program-1/resources/permission-share-personal-information-form
Linked from: https://inclusionagencynswact.org.au/for-families
403 Forbidden	
https://ocg.nsw.gov.au/safe-series-participant-resources
Linked from: https://inclusionagencynswact.org.au/about/big-situations/big-situations-critical-incidents
404 Not found	
https://inclusionagencynswact.org.au/about/big-situations/big-situations-child-safety-and-wellbeing
Linked from: https://inclusionagencynswact.org.au/about/big-situations/big-situations-family-violence
404 Not found	
https://inclusionagencynswact.org.au/about/big-situations/Birdie stories book order form | Children’s Health Queensland
Linked from: https://inclusionagencynswact.org.au/about/big-situations/big-situations-natural-disasters

Host not found	
https://www.dese.gov.au/child-care-package/resources/inclusion-support-program-isp-guidelines
Linked from
https://inclusionagencynswact.org.au/
https://inclusionagencynswact.org.au/about/the-inclusion-support-program

## Maintenance report for Dec 2022
* Craft CMS and plugins are updated to the latest version as below:
    - craft 3.7.53.1 => 3.7.61
    - blitz 3.12.6 => 3.12.7
    - freeform 3.13.18 => 3.13.22.1
    - imager-x v3.6.3.1 => v3.6.5
    - retour 3.2.1 => 3.2.7
    - seomatic 3.4.37 => 3.4.45

* cPanel: 
  File Usage: 144,905 / 600,000 (24.15%) , 
  Physical Memory Usage: 180.97 MB / 4 GB (4.42%)
* cPanel: error logs , all good
* CMS: content blocks, resources, Special Equipment Library ,SEO functionalities, forms - all good
* Database auto backup - running fine
* Error logs - all good
* Check Google Search Console - all done
* Broken links:
Host not found	
https://reporter.childstory.nsw.gov.au/s/mrg
Linked from: https://inclusionagencynswact.org.au/about/big-situations/big-situations-critical-incidents
	
403 Forbidden	
https://ocg.nsw.gov.au/safe-series-participant-resources
Linked from: https://inclusionagencynswact.org.au/about/big-situations/big-situations-critical-incidents
	
404 Not found	
https://inclusionagencynswact.org.au/about/big-situations/big-situations-child-safety-and-wellbeing
Linked from: https://inclusionagencynswact.org.au/about/big-situations/big-situations-family-violence
	
404 Not found	
https://inclusionagencynswact.org.au/about/big-situations/Birdie stories book order form | Children’s Health Queensland
Linked from: https://inclusionagencynswact.org.au/about/big-situations/big-situations-natural-disasters

403 Forbidden	
https://www.education.gov.au/inclusion-support-program-1/resources/overview-strategic-inclusion-plan
Linked from: https://idfm.org.au/strategic-inclusion-plan
	
404 Not found	
https://docs.education.gov.au/documents/inclusion-support-program-and-child-care-subsidy-eligibility-requirements
Linked from: https://idfm.org.au/for-services
	
404 Not found	
https://www.idfm.org.au/WWW_IDFM/media/Media/IDF-Complaints-and-Appeals-Process_2.pdf
Linked from: https://idfm.org.au/for-services
	
404 Not found	
https://www.idfm.org.au/WWW_IDFM/media/Media/Claiming-IDF-Subsidy-Fact-Sheet-Sept-2021-(003).pdf
Linked from: https://idfm.org.au/funding-streams/immediate-time-limited/claim

## 12 Sep 2022 by Azadeh

* Craft CMS and plugin update
  "craftcms/cms": "3.7.44" => "3.7.53.1",
  "craftcms/redactor": "2.10.8" => "2.10.10",
  "nystudio107/craft-retour": "3.1.72" => "3.2.1",
  "nystudio107/craft-seomatic": "3.4.31" => "3.4.37",
  "putyourlightson/craft-blitz": "3.12.4" => "3.12.6",
  "solspace/craft-freeform": "3.13.7" => "3.13.13" => "3.13.18",
  "spacecatninja/imager-x": "v3.6.1" => "v3.6.3.1",
  "spicyweb/craft-neo": "2.13.11" => "2.13.15",
  "verbb/field-manager" "2.2.4" => "2.2.5",
  "verbb/tablemaker": "2.0.1" => "3.0.0" => "3.0.4",
* Fix deprecated errors
* CMS: content blocks, resources, Special Equipment Library ,SEO functionalities, forms - all good
* Database auto backup is running
* Error logs - all good
* Check Google Search Console - all done
* Broken links:

403 Forbidden	
https://www.dese.gov.au/inclusion-support-program-1/resources/overview-strategic-inclusion-plan
Linked from: https://idfm.org.au/strategic-inclusion-plan
	
404 Not found	
https://docs.education.gov.au/documents/inclusion-support-program-and-child-care-subsidy-eligibility-requirements
Linked from: https://idfm.org.au/for-services
	
404 Not found	
https://www.idfm.org.au/WWW_IDFM/media/Media/IDF-Complaints-and-Appeals-Process_2.pdf
Linked from: https://idfm.org.au/for-families
https://idfm.org.au/for-services
	
404 Not found	
https://idfm.org.au/resources/idf-innovative-solutions-information-sheet-september-2021
Linked from: https://idfm.org.au/important-information
	
Timeout	
http://www.ku.com.au/resources/other/G_Privacy Statement.pdf
Linked from: https://idfm.org.au/privacy-policy
	
404 Not found	
https://www.idfm.org.au/WWW_IDFM/media/Media/Claiming-IDF-Subsidy-Fact-Sheet-Sept-2021-(003).pdf
Linked from: https://idfm.org.au/funding-streams/immediate-time-limited/claim
	
404 Not found	
https://www.idfm.org.au/innovative-solutions/resources/2017/idf-innovative-solutions-information-sheet-–-septe
Linked from: https://idfm.org.au/funding-streams/innovative-solutions/apply
	
404 Not found	
https://docs.education.gov.au/node/53244
Linked from: https://idfm.org.au/funding-streams/innovative-solutions/apply
	
404 Not found	
https://www.idfm.org.au/getattachment/Additional-Educator/Changes/IDFM-Final-Review-Form-August-2016-(web).pdf.aspx?lang=en-AU
Linked from: https://idfm.org.au/funding-streams/additional-educator/changes
	
404 Not found	
https://www.idfm.org.au/getattachment/FDC-Top-Up/Changes/IDFM-Final-Review-Form-August-2016-(web).pdf.aspx?lang=en-AU
Linked from: https://idfm.org.au/funding-streams/family-day-care-top-up/changes

## 16 June 2022 by Parry Huang

* Craft CMS and plugin update
  "craftcms/cms": "3.7.37" => "3.7.44",
  "craftcms/redactor": "2.10.5" => "2.10.8",
  "nystudio107/craft-retour": "3.1.70" => "3.1.72",
  "nystudio107/craft-seomatic": "3.4.28" => "3.4.31",
  "putyourlightson/craft-blitz": "3.11.2" => "3.12.4",
  "solspace/craft-freeform": "3.13.7" => "3.13.13",
  "spacecatninja/imager-x": "v3.5.6.1" => "v3.6.1",
  "spicyweb/craft-neo": "2.13.3" => "2.13.11",
  "verbb/tablemaker": "2.0.1" => "3.0.0",
* Fix deprecated errors
* Check auto-backup is running correctly - Yes
* Checked Craft log for errors - nothing to be concerned
* Testing
  - Test Multi-site CMS Functionalities
  - Test content blocks
  - Test Front end templates
  - Test resources
  - Test Special Equipment Library
  - Test SEO functionalities
  - Test Contact Form
* Checked server usage historical stats - no problem
* Added Global SEO title, description, and image description
* Added Content SEO keyword source, image description source
* Check page view and video tracking events traffics are collected in Google Analytics correctly - yes
* Check Google Search Console for any coverage issues found the following:
  Resubmitted the following sitemaps, as they are now correctly processed by search console, they will be processed by Google in the following weeks:
  * https://inclusionagencynswact.org.au//sitemaps-1-section-specialistEquipmentDetail-2-sitemap.xml
  * https://inclusionagencynswact.org.au//sitemaps-1-section-pages-2-sitemap.xml
* Scan for broken links 
  https://idfm.org.au broken links:
  * https://www.dese.gov.au/inclusion-support-program-1/resources/overview-strategic-inclusion-plan
    Linked from: https://idfm.org.au/strategic-inclusion-plan
  * https://docs.education.gov.au/documents/inclusion-support-program-and-child-care-subsidy-eligibility-requirements
    Linked from: https://idfm.org.au/for-services
  * https://www.idfm.org.au/WWW_IDFM/media/Media/How-to-clear-cache-Internet-explorer.pdf
    Linked from: https://idfm.org.au/for-services
  * https://www.idfm.org.au/WWW_IDFM/media/Media/IDF-Complaints-and-Appeals-Process_2.pdf
    Linked from: https://idfm.org.au/for-families and 1 more
  * https://www.idfm.org.au/WWW_IDFM/media/Media/How-to-clear-cache-Firefox.pdf
    Linked from: https://idfm.org.au/for-services
  * https://www.idfm.org.au/WWW_IDFM/media/Media/How-to-clear-cache-Google-Chrome.pdf
    Linked from: https://idfm.org.au/for-services
  * http://www.ku.com.au/resources/other/G_Privacy Statement.pdf
    Linked from: https://idfm.org.au/privacy-policy
  * https://www.idfm.org.au/WWW_IDFM/media/Media/Claiming-IDF-Subsidy-Fact-Sheet-Sept-2021-(003).pdf
    Linked from: 
    https://idfm.org.au/funding-streams/additional-educator/claim
    https://idfm.org.au/funding-streams/immediate-time-limited/claim
    https://idfm.org.au/funding-streams/family-day-care-top-up/claim
  * https://www.idfm.org.au/getattachment/Additional-Educator/Changes/IDFM-Final-Review-Form-August-2016-(web).pdf.aspx?lang=en-AU
    Linked from: https://idfm.org.au/funding-streams/additional-educator/changes
  * https://www.idfm.org.au/innovative-solutions/resources/2017/idf-innovative-solutions-information-sheet-–-septe
    Linked from: https://idfm.org.au/funding-streams/innovative-solutions/apply
  * https://docs.education.gov.au/node/53244Redirected
    Linked from: https://idfm.org.au/funding-streams/innovative-solutions/apply
  https://inlusionagencynswact.org.au broken links: 
  No broken links found
  https://inclusionsupportqld.org.au broken links:
  No broken links found
* Found a visual bug:
  when page contents are not enough to be well over user's browser height, e.g. https://idfm.org.au/docs/links-and-buttons on a 1040px screen. 
  The sticky header will prevent user scroll to footer. 
  This should be a css and js issue. Probably requires extra budget to fix.

## 30 March 2022 by Wei Lin

* Update Craft to 3.7.37
* Update all plugin to the latest
* Check auto-backup is running correctly - Yes
* Checked Craft log for errors - nothing to be concerned
* Testing
	- Test Multi-site CMS Functionalities
	- Test content blocks
	- Test Front end templates
	- Test resources
	- Test Special Equipment Library
	- Test SEO functionalities
	- Test Contact Form
* Checked server usage historical stats - no problem 
* Double check page view traffics are collected in Google Analytics correctly - yes.
* Check Google Search Console for any coverage issues - all good.
* Enable First Name, Last Name and Email field for the contact form submission view for all three sites, so it's clearer for admins to identify a submission.
* Security improvements
	- Prevent cache poisoning vulnerability
	- Enforce samesitecookievalue
	- preventUserEnumeration
* Scan for broken links - all good

Next steps for you / Issues found:
* We noticed that the NSW/ACT site admins group also has access to IDFM content (e.g. entries and categories). Is that a miss from us or is that something you intentionally changed? If that's not supposed to be like this, we can fix it.
* Legitimate form submissions can be caught up as spam and won't be able to proceed. We have changed the spam handling method, so if Google sees the submission as potential spam, it won't block them. Instead, the system will save the spam submissions in the Spam folder, where you can find in Freeform -> Spam. The downside is that the spam submissions won't trigger an email notifications to you. We recommend keep an eye on the spam folder regularly (e.g. once per week and see if any legitimate submissions are caught up there). We have also decrease the score which Google may see as spam from 0.5 to 0.4, so legitimate users are slightly less likely to be caught up as spam.
