# Yumpcraft

Node version: 14.17.0
Composer version: 2.1.3

## Staging
staging.idfm.org.au (idfm.test.yump.com.au prior to launch)

## Useful Site Links
IA - https://docs.google.com/spreadsheets/d/1ZwWRx0rgicIEkALdvZKLDiUD1OfaqNX_QY7q4aTWI4k/edit#gid=981782270

## Site Navigation Cache
- To clear the nav cache you can empty the contents of (not delete) yump\private\cache\data\$steID\site-navigation.json
- See more on cache here modules\yumpmodule\src\services\YumpModuleCacheService.php
- There are currently three sites so there needs to be three folders (1,2,3) here yump\private\cache\data\$steID\

## Multisite

### Local setup

You will need to setup the other two sites with virtual hosts and add to your .env files these variables (or whatever you set your domains up as locally):

IDFM_BASE_SITE_URL = "http://idfm.test"
NSW_BASE_SITE_URL = "http://inclusionagencynswact.test"
QLD_BASE_SITE_URL = "http://inclusionsupportqld.test"

IDFM_BASE_SITE_HOST = "idfm.test"
NSW_BASE_SITE_HOST = "inclusionagencynswact.test"
QLD_BASE_SITE_HOST = "inclusionsupportqld.test"

### Staging Setup
IDFM_BASE_SITE_URL = "https://idfm.test.yump.com.au"
NSW_BASE_SITE_URL = "https://inclusionagencynswact.test.yump.com.au"
QLD_BASE_SITE_URL = "https://inclusionsupportqld.test.yump.com.au"

IDFM_BASE_SITE_HOST = "idfm.test.yump.com.au"
NSW_BASE_SITE_HOST = "inclusionagencynswact.test.yump.com.au"
QLD_BASE_SITE_HOST = "inclusionsupportqld.test.yump.com.au"

### Production Setup
IDFM_BASE_SITE_URL = "https://idfm.org.au"
NSW_BASE_SITE_URL = "https://inclusionagencynswact.org.au"
QLD_BASE_SITE_URL = "https://inclusionsupportqld.org.au"

IDFM_BASE_SITE_HOST = "idfm.org.au"
NSW_BASE_SITE_HOST = "inclusionagencynswact.org.au"
QLD_BASE_SITE_HOST = "inclusionsupportqld.org.au"

## GA and Tag Manager
Inactive locally (\config\seomatic.php) but otherwise uses variables from .env

### Staging
IDFM
STAGING GA_IDFM_ID = "UA-5796028-8"
STAGING GTM_IDFM_ID = "GTM-N9VX768"

NSW
STAGING GA_NSW_ID = "UA-5796028-9"
STAGING GTM_NSW_ID = "GTM-MM5H3LV"

QLD
STAGING GA_QLD_ID = "UA-5796028-10"
STAGING GTM_QLD_ID = "GTM-P3NZMK2"

### Production

IDFM
PRODUCTION GA_IDFM_ID = "UA-5796028-11"
PRODUCTION GTM_IDFM_ID = "GTM-K7XVGXV"

NSW
PRODUCTION GA_NSW_ID = "UA-5796028-12"
PRODUCTION GTM_NSW_ID = "GTM-MN97KTM"

QLD
PRODUCTION GA_QLD_ID = "UA-5796028-13"
PRODUCTION GTM_QLD_ID = "GTM-NF4N5H6"


## Site Search
Site search has been updated to accomodate both entries and assets. Also it's re-written in Vue.js.

Prerequisites
- Asset Volume in the CMS with 'documents' as handle. Also with 'title', 'summary', 'tags' as fields.
- Tags (handle: tags) field added in CMS. 
- The handle of the 'tags' field can be changed in general.php "searchTagFieldHandle"
- "searchFiltersMapping" under general.php is manually set based on different projects
- 'q' as the query parameter in the search forms. That's also the only form param needed. The filters are now handled by Vue
- You will probably need to update 'getBreadcrumb' under YumpModuleService based on the filters of the project
- You will probably need to update the filters in search.html and also site-search.js based on the filters.
- Check out the available attributes per search result item using Vue browser tool to use anything you need for a project. E.g. tags, file extension, etc
