# select_backup.sh
#!/bin/bash

BACKUP_DIR=$1

if [ -z "$BACKUP_DIR" ]; then
    echo "Backup directory not specified."
    exit 1
fi

echo "Available backup files in $BACKUP_DIR:"
select FILE in $(ls $BACKUP_DIR/*.sql.zip $BACKUP_DIR/*.sql 2>/dev/null); do
    if [ -n "$FILE" ]; then
        echo "Selected file: $FILE"
        echo $FILE > /tmp/selected_backup_file.txt
        break
    else
        echo "Invalid selection. Please try again."
    fi
done
