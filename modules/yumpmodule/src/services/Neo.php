<?php
namespace modules\yumpmodule\services;

use benf\neo\elements\Block;

trait Neo{

    /**
     * @param $index
     * @param Block $block
     * @return bool
     */
    public function shouldHaveSidebar($index, Block $block)
    {
        $typeId = $this->getConfig("blockTypeIdWithSidebar");
        //first and the block type is
        if($index == 1 && $block->typeId == $typeId){
            return true;
        }
        return false;
    }

    /**
     * @param $blocks
     * @param $entryTypeId
     * @return array
     */
    public function maybeForceArticleBlockFirst($blocks, $entryTypeId) {
        $entryTypeIdArr = $this->getConfig('entryTypeIdsForceArticleContentFirst');
        if(in_array($entryTypeId, $entryTypeIdArr)){
            $index = $this->getFirstArticleBlockIndex($blocks);
            return array_slice($blocks, $index);
        }
        return $blocks;
    }

    /**
     * @param $blocks
     * @return int|null
     */
    public function getFirstArticleBlockIndex($blocks){
        $typeId = $this->getConfig("blockTypeIdWithSidebar");
        foreach ($blocks as $index => $block){
            if($block->typeId == $typeId){
                return $index;
            }
        }

        return null;
    }
}
