<?php
namespace modules\yumpmodule\services;

use Craft;

trait helper{
    /**
     * Craft3's config service doesn't have a 'get' function which you can retrieve an attribute without exceptions thrown. Now in Craft 3, if attribute is not found in config file, it will throw a UnknownPropertyException error. So we wrote our custom method to 'safely' get the config attributes.
     * @param  [type] $attrName [description]
     * @param  string $category [e.g. general, db, yump, etc]
     * @return [mixed|null]     return null if not found
     */
    public function getConfig($attrName, $category = 'custom') {
        try {
            return Craft::$app->config->$category->$attrName;
        } catch (\Exception $e) {}
        // } catch (\yii\base\UnknownPropertyException $e) {}
        return null;
    }
}
