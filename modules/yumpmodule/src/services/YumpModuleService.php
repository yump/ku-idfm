<?php

/**
 * yump module for Craft CMS 3.x
 *
 * Yump module for Craft 3
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2019 Yump
 */

namespace modules\yumpmodule\services;

use Craft;
use craft\base\Component;
use craft\elements\Asset;
use craft\elements\Category;
use craft\elements\Entry;
use craft\helpers\ElementHelper;
use craft\helpers\Queue;
use modules\yumpmodule\gears\cache\adapters\Nav as YumpNavAdapter;
use modules\yumpmodule\jobs\NavRefresher;
use modules\yumpmodule\YumpModule;
use spacecatninja\imagerx\ImagerX as Imager;

/**
 * YumpModuleService Service
 *
 * All of your module’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other modules can interact with.
 *
 * This class is directly connected with YumpModuleVariable. So if we call a function from Twig like this: craft.yump.exampleService, we will look for function with the same name in YumpModuleService, if that's missing in YumpModuleVariable. However, sometimes we don't want devs to accidentally call a function in YumpModuleService which make an curl request, or do something with the database. This is when this YumpModuleBlackboxService comes from. It means it can only be accessed by PHP files. This is kinda the same concept as separating Variable from Service, but one downside of variable is that PHP cannot access functions in Variable. That's why we have the general service (YumpModuleService), which can be access from both Twig and PHP.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Yump
 * @package   YumpModule
 * @since     1.0.0
 */
class YumpModuleService extends Component
{
    use traits\Video;
    use Neo;
    use Helper;

    private $_cachedNavPhpArray;
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin/module file, call it like this:
     *
     *     YumpModule::$instance->yump->exampleService()
     *
     * From Twig template to access functions in this file: craft.yump.exampleService()
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';

        return $result;
    }

    public function getIOSDevice()
    {
        $device = false;
        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'Macintosh') !== false) {
                $device = 'mac';
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone') !== false) {
                $device = 'iPhone';
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) {
                $device = 'iPad';
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPod') !== false) {
                $device = 'iPod';
            }
        }

        return $device;
    }

    public function ieVersion()
    {
        $version = false;
        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.') !== false) {
                $version = 6;
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.') !== false) {
                $version = 7;
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 8.') !== false) {
                $version = 8;
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 9.') !== false) {
                $version = 9;
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 10.') !== false) {
                $version = 10;
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false) {
                $version = 11;
            }
        }

        return $version;
    }

    /**
     * get all entries under section (with section handle provided)
     * @param  string $sectionHandle
     * @return array of entries
     *
     * Usage example (in PHP): Craft::$app->yump->getAllEntriesBySectionHandle($handle);
     *
     * To use in Twig template, use this:
     * craft.yump.getAllEntriesBySectionHandle($sectionHandle);
     *
     * Or use craft built-in:
     * <% craft.entries.section( $sectionHandle ).find() %>
     */
    public function getAllEntriesBySectionHandle($sectionHandle, $limit = null)
    {
        return craft\elements\Entry::find()
            ->section($sectionHandle)
            ->limit($limit)
            ->all();
    }

    /**
     * NOTE: currently only works for text and textarea, might extend it to other types in the future.
     *
     * @param  [FieldModel] $field
     * @return [string]
     */
    private function _determineFieldType($field)
    {
        if ($field->type == 'PlainText') {
            if (empty($field->settings['multiline'])) {
                return 'text';
            } else {
                return 'textarea';
            }
        }

        return '';
    }

    public function getUserGroupIdByHandle($userGroupHandle)
    {
        $groupId = Craft::$app->userGroups->getGroupByHandle($userGroupHandle)->id;
        return $groupId;
    }

    public function getSectionIdByHandle($sectionHandle)
    {
        $sectionId = Craft::$app->sections->getSectionByHandle($sectionHandle)->id;
        return $sectionId;
    }

    /**
     * Generate validated URL from admin input (e.g. sometimes admins might accidentally put in whitespaces, or forget the protocol)
     * @param  [type] $url [description]
     * @return [type]      [description]
     */
    public function generateValidUrl($url) {
        if(!empty($url)) {
            $url = trim($url);

            // excluding url starting with mailto:, tel:, # and /
            if(substr($url, 0, 1) == "/" or substr($url, 0, 1) == '#' or substr($url, 0, 7) == 'mailto:' or substr($url, 0, 4) == 'tel:') {
                return $url;
            }

            if(substr( $url, 0, 7 ) !== "http://" AND substr( $url, 0, 8 ) !== "https://") {
                $url = '//' . $url;
            }
            return $url;
        }
    }

    /**
     * Surprised, we get ElementCriteriaModel instead of EntryModel if we call EntriesService->getEntryById in Twig! and the result is wrong, i guess it couldn't find the entry using the ID we supplied, (cuz the passed-in $entryId was tested as null for some reasons). Anyway, I just create this fix function for twig to use.
     * @param  [type] $entryId  [description]
     * @param  [type] $localeId [description]
     * @return [type]           [description]
     */
    public function getEntryByIdFix($entryId, $localeId = null)
    {
        if (is_numeric($entryId)) {
            return Craft::$app->entries->getEntryById((int) $entryId, $localeId);
        }
    }

    /**
     * [getFlashMessage description]
     * @param  string $handle default: 'error'; also, 'notice'
     * @return String
     */
    public function getFlashMessage($handle = 'error')
    {
        return Craft::$app->userSession->getFlash($handle);
    }

    public function addHttpSessionVariable($key, $value)
    {
        Craft::$app->httpSession->add($key, $value);
    }

    public function isStaging()
    {
        return strpos($_SERVER['SERVER_NAME'], 'staging');
    }

    public function isDev()
    {
        return strpos($_SERVER['SERVER_NAME'], '.dev') or strpos($_SERVER['SERVER_NAME'], '.test');
    }

    public function isProduction()
    {
        return !$this->isStaging() and !$this->isDev();
    }

    public function getGlobalFieldValue($globalSetHandle, $fieldHandle)
    {
        $settings = Craft::$app->globals->getSetByHandle($globalSetHandle);

        if (!empty($settings)) {
            return $settings->$fieldHandle;
        } else {
            return null;
        }
    }

    /**
     * Get some useful data for server error debugging (e.g. send email notification to devs)
     * @return Array
     */
    public function getHttpRequestData()
    {
        $data = [];
        $requestType = Craft::$app->request->getRequestType();
        if ($requestType == 'GET') {
            $params = $_GET;
        } else if ($requestType == 'POST') {
            $params = $_POST;
        } else {
            $params = Craft::$app->request->getRestParams();
        }
        $isAjax = Craft::$app->request->isAjaxRequest();

        $data['url'] = Craft::$app->request->url;
        $data['requestType'] = $requestType;
        $data['ip'] = Craft::$app->request->getIpAddress();
        $data['hostname'] = gethostbyaddr($data['ip']);
        $data['isAjax'] = $isAjax ? 'Yes' : 'No';
        $data['contentType'] = Craft::$app->request->getMimeType();
        $data['params'] = print_r($params, true);

        $data = array_merge($data, getallheaders());

        $data['sessionVars'] = print_r($_SESSION, true);

        return $data;
    }

    /**
     * get array of values / labels of the value of a multi-choice field
     * @param  EntryModel  $entry
     * @param  string  $fieldHandle
     * @param  boolean $getAssociatedArray    get associated array version of results. If this is set to true, then $getLabel becomes redundant
     * @param  boolean $getLabel    get array of labels, otherwise, get array of values
     * @return array               array of string
     */
    public function getArrayOfMultiSelectOptions($entry, $fieldHandle, $getAssociatedArray = false, $getLabel = false)
    {
        $result = [];
        foreach ($entry->$fieldHandle as $option) {
            if ($getAssociatedArray) {
                $result[$option->value] = $option->label;
            } else {
                if ($getLabel) {
                    $result[] = $option->label;
                } else {
                    $result[] = $option->value;
                }
            }
        }
        return $result;
    }

    /** return site url with protocol and server name (without back slash at the end) */
    public function getSiteUrl()
    {
        return sprintf(
            "%s://%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME']
        );
    }

    public function truncate($string, $length = 100, $append = "...")
    {
        $string = trim($string);

        if (strlen($string) > $length) {
            $string = wordwrap($string, $length);
            $string = explode("\n", $string, 2);
            $string = $string[0] . $append;
        }

        return $string;
    }

    /**
     * ***************************************************************************************
     * In PHP, some functions throws notice / errors instead of exceptions.
     * If we DO want to catch those, we can use the following function. However, remember to restore the error handle when its done, no matter success or not.
     * The code to restore error handler: either use the restoreErrorHandler function underneath if can't remember, or use PHP restore_error_handler();
     *
    Example:
    set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
    // error was suppressed with the @-operator
    if (0 === error_reporting()) {
    return false;
    }

    throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
    });

    try {
    $contentArr = unserialize($content); // the code we want to make sure it wouldn't stop app
    restore_error_handler();
    } catch (\Exception $e) {
    print_r("Things go wrong!");
    restore_error_handler();
    }
     */
    public function setErrorHandler()
    {
        set_error_handler(function ($errno, $errstr, $errfile, $errline, array $errcontext) {
            // error was suppressed with the @-operator
            if (0 === error_reporting()) {
                return false;
            }

            throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
        });
    }

    public function restoreErrorHandler()
    {
        restore_error_handler();
    }

    public function getGtmId()
    {
        // new way of getting GTM ID: via SEOMatic settings
        $gtmId = $this->getSiteIdentityAttribute('googleTagManagerID');
        if ($gtmId) {
            return $gtmId;
        }

        // if not set, try to get it from the old way: defined in globals

        // old way of getting GTM IDs
        $matrix = Craft::$app->yump->getGlobalFieldValue('externalScripts', 'externalScripts');

        if ($matrix) {
            foreach ($matrix as $block) {
                if ($block->type == 'googleTagManager' and !empty(trim($block['googleTagManagerId']))) {
                    return trim($block['googleTagManagerId']);
                }
            }
        }
    }

    public function getMicrotimeAsId()
    {
        return str_replace(" ", "-", str_replace(".", "-", microtime()));
    }

    public function getFirstname($fullName)
    {
        $nameArr = explode(' ', $fullName, 2);
        if (count($nameArr) >= 1) {
            return $firstName = $nameArr[0];
        }
    }

    /**
     * insert ',' into numbers
     */
    public function formatNumber($number)
    {
        if (!$number) {
            return $number;
        }

        return number_format($number);
    }

    public function hexToRgb($hex, $returnAsArray = false)
    {
        try {
            list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
            if ($r && $g && $b) {
                if ($returnAsArray) {
                    return [
                        'R' => $r,
                        'G' => $g,
                        'B' => $b,
                    ];
                } else {
                    return $r . ', ' . $g . ', ' . $b;
                }
            }
        } catch (\Exception $e) {
        }
    }

    public function isInCategory($element, $categoryFieldHandle, $categorySlug)
    {
        if (!empty($element[$categoryFieldHandle])) {
            foreach ($element[$categoryFieldHandle] as $category) {
                if ($category->slug == $categorySlug) {
                    return true;
                }
            }
        }
        return false;
    }

    public function getTelHref($number)
    {
        $temp = str_replace(' ', '-', trim($number));
        $temp = str_replace('(', '', $temp);
        return str_replace(')', '', $temp);
    }

    /**
     * Hmm, maybe the cache thing is useless?
     *
     * Check if a plugin has been loaded and enabled, and cached the result in this class. But it only caches it if it is already loaded and enabled, otherwise, it will check again next time when called.
     * @param  [type]  $pluginHandle [description]
     * @return boolean               true / false
     */
    public function isPluginLoaded($pluginHandle)
    {
        if (!empty($this->cachedCheckedPlugins[$pluginHandle])) {
            return true;
        } else {
            $plugin = Craft::$app->plugins->getPlugin($pluginHandle);
            if ($plugin) {
                $this->cachedCheckedPlugins[$pluginHandle] = true;
                return true;
            }
        }

        return false;
    }

    /**
     * SEOmatic data helper
     * get the social media handle (username), given the name of social media. Social settings are set up in SEOmatic
     * @param  [string] $socialMediaName: Strictly restricted to the following strings (case sensitive):
     * 'facebook', 'twitter', 'linkedIn', 'googlePlus', 'youtube', 'youtubeChannel', 'instagram', 'pinterest', 'github', 'vimeo'
     * @return [string/null]
     */
    public function getSocialMediaHandle($socialMediaName)
    {
        if ($this->isPluginLoaded('seomatic')) {
            $socialSettings = Craft::$app->seomatic->getSocial(Craft::$app->language);
            if (!empty($socialSettings[$socialMediaName . 'Handle'])) {
                return $socialSettings[$socialMediaName . 'Handle'];
            }
        }
    }

    /**
     * SEOmatic data helper
     * get one specific attribute set up in SEOmatic's Site Identity section
     * @param  [string] $attr [description]
     * @return [mixed/null]       [description]
     */
    public function getSiteIdentityAttribute($attr)
    {
        if ($this->isPluginLoaded('seomatic')) {
            $identity = Craft::$app->seomatic->getIdentity(Craft::$app->language);
            if (!empty($identity[$attr])) {
                return $identity[$attr];
            }
        }
    }

    public function highlight($c, $q)
    {
        $q = str_replace(array('', '\\', '+', '*', '?', '[', '^', ']', '$', '(', ')', '{', '}', '=', '!', '<', '>', '|', ':', '#', '-', '_'), '', $q);
        $c = preg_replace("/($q)(?![^<]*>)/i", "<span class=\"highlight\">\${1}</span>", $c);
        // $q=explode(' ',str_replace(array('','\\','+','*','?','[','^',']','$','(',')','{','}','=','!','<','>','|',':','#','-','_'),'',$q));
        // for($i=0;$i<sizeOf($q);$i++)
        //     $c=preg_replace("/($q[$i])(?![^<]*>)/i","<span class=\"highlight\">\${1}</span>",$c);
        return $c;
    }

    public function searchExcerpt($text, $phrase, $radius = 150, $ending = "...")
    {

        //$text =  preg_replace("/<img[^>]+\>/i", "", $text);
        $text = strip_tags($text);

        $phraseLen = strlen($phrase);

        //don't let the radius be less than the phrase that searched for
        if ($radius < $phraseLen) {
            $radius = $phraseLen;
        }
        $phrases = explode(' ', $phrase);

        //search for instances
        foreach ($phrases as $phraseUnit) {
            $pos = strpos(strtolower($text), strtolower($phraseUnit));
            if ($pos > -1) {
                break;
            } else {
                return false;
            }
        }

        $startPos = 0;
        if ($pos > $radius) {
            $startPos = $pos - $radius;
        }

        $textLen = strlen($text);

        $endPos = $pos + $phraseLen + $radius;
        if ($endPos >= $textLen) {
            $endPos = $textLen;
        }

        $excerpt = substr($text, $startPos, $endPos - $startPos);
        if ($startPos != 0) {
            $excerpt = substr_replace($excerpt, $ending, 0, $phraseLen);
        }

        if ($endPos != $textLen) {
            $excerpt = substr_replace($excerpt, $ending, -$phraseLen);
        }

        //highlight
        $excerpt = $this->highlight($excerpt, $phrase);

        return $excerpt;
    }

    /**
     * Look for query in data recursively.
     * @param  [type] $query          [description]
     * @param  [type] $object         [description]
     * @param  array  $fieldsToSearch basically most of the plain text, redactor type fields. Reminder to check the fields inside matrix fields as well, because they are not immediately visible in the field list in Craft
     * @return [type]                 [description]
     */
    public function deepBodySearch($query, $object, $fieldsToSearch = array('richText', 'summary', 'subHeading'))
    {

        // echo "<pre>";
        // echo get_class($object) . "\n";
        // echo "</pre>";

        // $elementType = @$object->getElementType();
        // if($elementType) {
        if ($object instanceof \craft\elements\Entry) {
            $block = @$object['body'];
        } else {
            $block = $object;
        }
        if ($block) {
            // print_r($block);
            if ($block instanceof \benf\neo\elements\Block or $block instanceof \craft\elements\MatrixBlock) {
                foreach ($fieldsToSearch as $field) {
                    if (!empty($block[$field])) {
                        $found = $this->searchExcerpt($block[$field], $query);
                        if ($found) {
                            return $found;
                        }
                    }
                }
            }

            // object of the neo field itself
            if ($block instanceof \benf\neo\elements\db\BlockQuery) {
                foreach ($block as $childBlock) {
                    $found = $this->deepBodySearch($query, $childBlock, $fieldsToSearch);
                    if ($found) {
                        return $found;
                    }
                }
            }

            if ($block instanceof \benf\neo\elements\Block and !empty($block['reusableSnippetsArticle'])) {
                $found = $this->deepBodySearch($query, $block['reusableSnippetsArticle'], $fieldsToSearch);
                if ($found) {
                    return $found;
                }
            }

            if ($block instanceof \benf\neo\elements\Block and !empty($block['reusableSnippetsFullWidth'])) {
                $found = $this->deepBodySearch($query, $block['reusableSnippetsFullWidth'], $fieldsToSearch);
                if ($found) {
                    return $found;
                }
            }

            if ($block instanceof \craft\elements\db\ElementQuery) {
                // reusable snippets
                if ($block->elementType == 'craft\elements\Entry') {
                    foreach ($block as $snippet) {
                        $found = $this->deepBodySearch($query, $snippet, $fieldsToSearch);
                        if ($found) {
                            return $found;
                        }
                    }
                }
                // elseif($block->elementType == 'benf\neo\elements\Block') {

                // }
            }

            // if($block instanceof \verbb\supertable\elements\SuperTableBlockElement) {
            //  print_r($block);
            // }

            /** This compactContentMatrix is used in Healthecare, it's unlikely to be used in future projects. However, you can replace this with any matrix field put in Neo if you want the search excerpt to work on the content of this matrix field as well. In the future at some point, maybe we can loop through the fields of a Neo_BlockModel and handle each type separately. */
            // if($block instanceof \benf\neo\elements\Block and !empty($block['compactContentMatrix'])) {
            //  $found = $this->deepBodySearch($query, $block['compactContentMatrix'], $fieldsToSearch);
            //  if($found) {
            //      return $found;
            //  }
            // }

            // if($block instanceof \craft\elements\db\ElementQuery and $block->getElementType()->getClassHandle() == 'MatrixBlock') {
            //  foreach ($block as $childMatrixBlock) {
            //      if($childMatrixBlock instanceof MatrixBlockModel) {
            //          $found = $this->deepBodySearch($query, $childMatrixBlock, $fieldsToSearch);
            //          if($found) {
            //              return $found;
            //          }
            //      }
            //  }
            // }
        }
        // }

        return false;
    }

    public function getLastWordInString($string)
    {
        $pieces = explode(' ', $string);
        return array_pop($pieces);
    }

    //  public function getVideoId($videoUrl) {
    //      $path = $_SERVER['DOCUMENT_ROOT'] . "/craft/plugins/videoembedutility/twigextensions/VideoEmbedUtilityTwigExtension.php";
    //      if(file_exists($path)) {
    //          try{
    //              require_once($path);
    //              $helper = new VideoEmbedUtilityTwigExtension();
    //              return $helper->videoId(trim($videoUrl));
    //          } catch (\Exception $e) {}
    //      }
    //  }

    /**
     * Check if a hash string is valid in URL
     * @param  [string] $hash
     * @return [trimmed URL | false if invalid]
     */
    public function validateUrlHash($hash)
    {
        $hash = trim($hash);
        $testUrl = 'http://superdummytest.com#' . $hash;
        if (filter_var($testUrl, FILTER_VALIDATE_URL) === false) {
            return false;
        }

        return $hash;
    }

    /**
     * Using Yump Thumbnailer instead of other thumbnailer like imager
     * @param  [type] $url    [description]
     * @param  [type] $config [description]
     * @return [type]         [description]
     */
    public function getYumpThumbnailerUrl($url, $config)
    {
        $url = str_replace('https://', '/https/', $url);
        $url = str_replace('http://', '/http/', $url);
        $url = str_replace('//', '/', $url);
        $url = trim($url);

        $mode = !empty($config['mode']) ? $config['mode'] : 'crop';
        $width = !empty($config['width']) ? $config['width'] : 9999;
        $height = !empty($config['height']) ? $config['height'] : 9999;

        return "/thumb/" . $width . "x" . $height . "/" . $mode . $url;
    }

    /**
     * Helper function to get a thumbnail of an image asset. Powered by imager
     *
     * Rules:
     * 1. for svg files, return the URL right away, because the transformation might alter the image in an unexpected way.
     * 2. if imager failed for any reasons, we use the Yump thumbnailer as fallback. Refer to getYumpThumbnailerUrl() function above.
     *
     * @param  \craft\elements\Asset  $image              [description]
     * @param  array  $transform          transform settings. re
     * @param  boolean $returnImagerObject whether we should return the \aelvan\imager\models\CraftTransformedImageModel object instead - only works for imager. By default, we will return the URL only.
     * @param  boolean $useYumpThumbnailerByDefault by default, yump thumbnailer is only used as fallback. But you can set this to true if you want to use it directly. Or, you can use the getYumpThumbnailerUrl() function above directly.
     * @return [type]                      [description]
     */
    public function getThumbnail($image, $transform, $returnImagerObject = false, $useYumpThumbnailerByDefault = false)
    {
        if (empty($image) or !($image instanceof \craft\elements\Asset) or $image->kind != 'image') {
            return null;
        }

        /** just return the URL if it's SVG */
        if (stripos($image->getMimeType(), 'svg') !== false) {
            return $image->getUrl();
        }

        if (!$useYumpThumbnailerByDefault) {
            try {
                // use the focal point if not applied yet
                if (empty($transform['position'])) {
                    $transform['position'] = $image->getFocalPoint();
                }

                // run imager
                $thumbnailObject = Imager::$plugin->imager->transformImage($image, $transform, null, null);

                if ($thumbnailObject) {
                    if ($returnImagerObject) {
                        return $thumbnailObject;
                    } else {
                        return $thumbnailObject->url;
                    }
                } else {
                    throw new \Exception("Got empty thumbnail by running imager->transformImage for unknown reasons.");
                }
            } catch (\Exception $e) {
                Craft::error("Failed to get thumbnail using imager. Error message: " . $e->getMessage(), __METHOD__);
            }
        }

        // use Yump Thumbnailer as fallback.
        return $this->getYumpThumbnailerUrl($image->getUrl(), $transform);
    }

    public function isNavItemActive($element)
    {
        if (!empty($element)) {
            $segments = Craft::$app->request->getSegments();
            $slug = $element->slug;
            if ($slug and in_array($slug, $segments)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Craft::$app->sections->getEntryTypesByHandle returns an array instead of just one. My guess is there could be entry types in different sections using the same handle. So, to get a unique entry type, we pass in a second param '$sectionHandle' to pinpoint one entry type model
     * @param  [type] $handle        [entry type handle]
     * @param  [type] $sectionHandle [section handle]
     * @return [EntryTypeModel|null]                [description]
     */
    //  public function getEntryTypeByHandle($handle, $sectionHandle) {
    //      $entryTypes = Craft::$app->sections->getEntryTypesByHandle($handle);
    //      if(!empty($entryTypes)) {
    //          foreach ($entryTypes as $entryType) {
    //              $section = $entryType->getSection();
    //              if($section and $section->handle == $sectionHandle) {
    //                  return $entryType;
    //              }
    //          }
    //      }
    //  }
    //
    //  public function getCategoriesByGroupHandle($categoryGroupHandle, $returnTitlesOnly = false) {
    //      $criteria = Craft::$app->elements->getCriteria(ElementType::Category);
    //      $criteria->group = $categoryGroupHandle;
    //      $criteria->limit = null;
    //      $categories = $criteria->find();
    //      if(!$returnTitlesOnly) {
    //          return $categories;
    //      } else {
    //          $categoryTitles = [];
    //          foreach ($categories as $category) {
    //              $categoryTitles[] = $category->title;
    //          }
    //          return $categoryTitles;
    //      }
    //  }

    /**
     * Extract (from Mailchimp form embedded code) the form action URL string and construct the bot hidden field value string based on that, so the admin only need to copy & paste in the entire embedded code without manually extract the two strings into CMS.
     * @return array|null if fail.
     * Example: array('action' => 'https://yump.us3.list-manage.com/subscribe/post?u=e4429d6397aaf2320b515bb93&amp;id=d68e372e21', 'botId' => 'b_e4429d6397aaf2320b515bb93_d68e372e21')
     */
    public function extractMailchimpFormData($embeddedCode)
    {
        $matches = array();
        preg_match('/<form action="(((?!").)+)"/', $embeddedCode, $matches);

        if (!empty($matches[1]) and strpos($matches[1], '"') === false) {
            $action = $matches[1];
            $m = [];
            preg_match('/u=([a-zA-Z0-9]+)&amp;id=([a-zA-Z0-9]+)/', $action, $m);

            if (!empty($m[1]) and !empty($m[2])) {
                $botId = 'b_' . $m[1] . '_' . $m[2];
                return [
                    'action' => $action,
                    'botId' => $botId,
                ];
            }
        }
    }

    /**
     * Craft put this 'p' param in the URL, we probably want to kill that to make the URL cleaner.
     * @return string: constructed URL params. E.g. query=test&location=vic
     */
    public function getUrlParams()
    {
        $paramString = '';
        $usableParams = [];
        foreach ($_GET as $key => $value) {
            if ($key != 'p') {
                $usableParams[] = $key . '=' . $value;
            }
        }
        $paramString = implode('&', $usableParams);

        return $paramString;
    }

    /**
     * The craft's craft.request.getSegments only works for current URL. But this function use URL as a param, so you can get segments of any URL.
     * @param  [string] $url [description]
     * @return [type]      [description]
     */
    public function getUrlSegments($url)
    {
        $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        return explode('/', $uri_path);
    }

    /**
     * Get excerpt from string
     *
     * @param String $str String to get an excerpt from
     * @param Integer $startPos Position int string to start excerpt from
     * @param Integer $maxLength Maximum length the excerpt may be
     * @return String excerpt
     */
    public function getExcerpt($str, $startPos = 0, $maxLength = 100)
    {
        if (strlen($str) > $maxLength) {
            $excerpt = substr($str, $startPos, $maxLength - 3);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt = substr($excerpt, 0, $lastSpace);
            $excerpt .= '...';
        } else {
            $excerpt = $str;
        }

        return $excerpt;
    }

    public function ljDynamicFieldToArray($fieldVal)
    {
        if (!empty($fieldVal)) {
            // so weird, in Live Preview, this value is automatically an array for some reasons
            if (is_array($fieldVal)) {
                return $fieldVal;
            }

            $arr = json_decode($fieldVal, true);
            if (json_last_error() === JSON_ERROR_NONE) {
                // ljDynamicField returns a single item (not array) if there's only one item is selected
                if (!is_array($arr)) {
                    $arr = [$arr];
                }

                return $arr;
            }
        }
        return null;
    }

    public function checkScriptPermission($allowAdmins = false, $allowUserGroups = array())
    {
        $PASSWORD = Craft::$app->config->general->controllerActionsPassword;
        $ALLOWED_IPs = Craft::$app->config->general->controllerActionsAllowedIps;

        // TODO: support $allowUserGroups as well

        return
            // is admin
            ($allowAdmins && Craft::$app->getUser() && Craft::$app->getUser()->isAdmin)
            or
            // password correct and IP is whitelisted
            (!empty($PASSWORD) &&
                !empty($ALLOWED_IPs) &&
                is_array($ALLOWED_IPs) &&
                Craft::$app->getRequest()->getParam('a') == $PASSWORD &&
                (in_array($_SERVER['REMOTE_ADDR'], $ALLOWED_IPs)
                    || (!empty($_SERVER['HTTP_CF_CONNECTING_IP']) && in_array($_SERVER['HTTP_CF_CONNECTING_IP'], $ALLOWED_IPs)))); // sometimes REMOTE_ADDR shows Cloudflare IPs if going through Cloudflare, but Cloudflare has another attribute telling us what the original IP is.
    }

    /**
     * Get a param which is passed to a route / template.
     * @param  [type] $paramName [description]
     * @return [type]            [description]
     */
    public function getRouteParam($paramName)
    {
        $params = Craft::$app->getUrlManager()->getRouteParams();
        return @$params[$paramName];
    }

    /**
     * Pretty dump a param and kill the process (if set the $terminate = true)
     * @param  [mixed] $param [description]
     * @return [type]        [description]
     */
    public function dump($param, $terminate = true)
    {
        echo "<pre>";
        print_r($param);
        echo "</pre>";

        if ($terminate) {
            exit();
        }
    }

    public function log($message, $logFileName = 'yump', $category = 'info')
    {
        $file = Craft::getAlias('@storage/logs/' . $logFileName . '.log');

        if (is_array($message)) {
            $message = print_r($message, true);
        }

        $log = date('Y-m-d H:i:s') . ' [' . strtoupper($category) . ']' . $message . "\n";

        \craft\helpers\FileHelper::writeToFile($file, $log, ['append' => true]);
    }

    /**
     * To render a template within a plugin. Sometimes it's hard to use include on twig, because the current template path might have changed already.
     *
     * HINT: you might want to apply the | raw filter on twig
     * @param  [type] $pluginHandle [description]
     * @param  [type] $path         [description]
     * @param  array  $data         [description]
     * @return [type]               [description]
     */
    public function displayPluginTemplate($pluginHandle, $path, $data = array())
    {
        try {
            $oldMode = Craft::$app->view->getTemplateMode();
            Craft::$app->view->setTemplateMode(\craft\web\View::TEMPLATE_MODE_CP);
            $html = Craft::$app->view->renderTemplate($pluginHandle . '/' . trim($path, '/'), $data);
            Craft::$app->view->setTemplateMode($oldMode);
            return $html;
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }
    }

    /**
     * In Craft 3, in some cases, it's impossible to just do $_POST[$paramName] = $value to set a request body param, because Craft might have getValidatedBodyParam() function to get a body param securely. If you look into that function, you will see Craft::$app->getSecurity()->validateData($value), which requires the data to be hashed.
     *
     * Now, it's NOT just a matter of doing $_POST[$paramName] = Craft::$app->getSecurity()->hashData($value), because if you look into the getBodyParam() function inside getValidatedBodyParam(), it gets the param value from getBodyParams(), which is likely to return a already cached _bodyParams array that has already been set (cached) in previous code.
     *
     * So, the proper way to override this request param in PHP is to get all the bodyParams, update that particular param we want in the array, and call setBodyParams to update the whole _bodyParams array again.
     * @param [string] $paramName [description]
     * @param [mixed] $value     [description]
     */
    public function setBodyParam($paramName, $value)
    {
        $params = Craft::$app->request->getBodyParams();
        $params[$paramName] = Craft::$app->getSecurity()->hashData($value);
        Craft::$app->request->setBodyParams($params);
    }

    public function extractOnlyNumbers($str)
    {
        return (int) filter_var($str, FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * Get all the data needed for rendering a banner.
     *
     * NOTE: if the project needs additional fields / settings, you should copy and paste this function into the project's module service, and modify the function to suit your own needs.
     *
     * @param  craft\elements\Entry $entry the entry which hosts the banner
     * @param text variation of banner
     * @return array: the banner attributes
     * Example return data:
    [
    'heading' => string: 'This is the heading',
    'summary' => null, or string: 'This is the summary',
    'buttons' => null, or array(
    typedlinkfield\models\Link,
    // typedlinkfield\models\Link, // up to two buttons
    ),
    'media' => null, or array(
    'type' => 'image' or 'video',
    'content' => craft\elements\Asset (limited to be image type), or the video field value by mikestecker\videoembedder plugin (if is video),
    // if media has special settings (currently only used by video)
    'settings' => [
    'transcriptEntry' => craft\elements\Entry (Video Transcript entry)
    'videoHtmlTitle' => string,
    ],
    ),
    ]
     */
    public function getBanner($entry, $variation = '')
    {
        if (!empty($entry['banner'])) {
            $banner = $entry->banner;
            // $this->dump($banner);
            $keepBannerRatio = $entry->keepBannerRatio;

            // get textContent block and media block
            $textContentBlock = null;
            $mediaBlock = null;
            foreach ($banner->level(1)->all() as $block) {
                $handle = $block->getType()->handle;
                if ($handle == 'textContent') {
                    $textContentBlock = $block;
                } else if ($handle == 'media') {
                    $mediaBlock = $block;
                }
            }

            // if not even textContent, the banner will be considered empty, even if media block is present.
            if ($textContentBlock) {
                // getting data of textContent
                $heading = $textContentBlock->heading;

                //set icon if present
                $icon = null;
                if (isset($textContentBlock['icon'])) {
                    $icon = $textContentBlock->icon->one();
                }
                //set banner text color
                $textColor = null;
                if (!empty(trim($textContentBlock->bannerTextColor))) {
                    $textColor = $textContentBlock->bannerTextColor;
                }
                // print_r($icon);

                // if heading not specified, use the entry's heading
                if (empty(trim($heading))) {
                    $heading = $entry->title;
                }
                $summary = null;
                $buttons = null;

				// For variation classes:
                $size = ' banner--size-default';
				$colour = '';
				$hasButton = '';
                $flip = '';
                $bgImage = null;

                if ($entry->type->handle = 'homepage') {
                    $size = ' banner--size-large';
                }

                foreach ($textContentBlock->getChildren()->all() as $block) {
					$handle = $block->getType()->handle;
                    if ($handle == 'summary') {
						$summary = $block->summary;
                        // if summary not specified, use entry's summary
                        if (empty($summary)) {
							$summary = !empty($entry['summary']) ? $entry->summary : null;
                        }
                    } else if ($handle == 'buttons') {
						if($block->cta) {
							$buttons[] = $block->cta;
							$hasButton = ' banner--has-button';
						}
                    }
                }

                // getting data of media
                $media = null;
                if ($mediaBlock) {
                    foreach ($mediaBlock->getChildren()->all() as $block) {
						//var_dump($mediaBlock->bannerBackgroundColor);
                        
                        // if($mediaBlock->bannerBackgroundColor!='') {
						// 	$colour = ' banner--colour-'.$mediaBlock->bannerBackgroundColor;
						// }
						// else 
                        if($mediaBlock->makeBannerBackgroundRed) {
							$colour = ' banner--colour-red';
						}
                        if($mediaBlock->flipWordingAndImageOnTheBanner) {
							$flip = ' banner--flip';
						}
                        
                        if (!empty($mediaBlock['bannerBackgroundImage'])) {
                            $bgImage = $mediaBlock->bannerBackgroundImage->one();
                        }
                        
                        $handle = $block->getType()->handle;
                        if ($handle == 'image') {
                            foreach ($block->getChildren()->all() as $childBlock) {
                                $childBlockHandle = $childBlock->getType()->handle;
                                $imageSource = null;
                                if ($childBlockHandle == 'useFeaturedImage') {
                                    if (!empty($entry['featuredImage'])) {
                                        $imageSource = $entry->featuredImage;
                                    }
                                } else if ($childBlockHandle == 'customImage') {
                                    if (!empty($childBlock['image'])) {
                                        $imageSource = $childBlock->image;
                                    }
                                }
                                if ($imageSource) {
                                    $image = $imageSource->one();
                                    if ($image) {
                                        $media = [
                                            'type' => 'image',
                                            'content' => $image,
                                        ];
                                    }
                                }
                            }
                        } else if ($handle == 'video') {
                            if (!empty($block['video'])) {
                                $media = [
                                    'type' => 'video',
                                    'content' => $block->video,
                                    'settings' => [
                                        'transcriptEntry' => $block->videoTranscript ? $block->videoTranscript->one() : null,
                                        'videoHtmlTitle' => $block->videoHtmlTitle,
                                    ],
                                ];
                            }
                        }
                    }
                }

				$variationClasses = $size . $colour . $hasButton . $flip;

                return [
                    'heading' => $heading,
                    'summary' => $summary,
                    'buttons' => $buttons,
                    'icon' => $icon,
                    'media' => $media,
                    'bgImage' => $bgImage,
                    'textColor' => $textColor,
                    'variationClasses' => $media ? 'banner--with-media ' . $variationClasses : 'banner--simple ' . $variationClasses,
                    'keepBannerRatio' => $keepBannerRatio ? ' banner--keep-ratio' : '',
                ];
            }
        }
    }

    /**
     * Get Hero With CTAs Neo Block
     *
     * @param craft\elements\Entry $entry
     * @return array
     */
    public function getHeroWithCtas($entry)
    {
        if (!empty($entry['heroImageWithCallsToAction'])) {
            $hero = $entry->heroImageWithCallsToAction;
            $ctaBlocks = [];
            $backgroundBlock = [];

            foreach ($hero->level(1)->all() as $block) {
                $handle = $block->getType()->handle;
                if ($handle == 'background') {
                    $backgroundBlock = $block;
                } else if ($handle == 'callsToAction') {
                    $ctaBlocks[] = $block;
                }
            }

            return [
                'background' => $backgroundBlock,
                'ctas' => $ctaBlocks,
            ];
        }
    }

    /**
     * Will return 3 cards from any custom selections below together with the latest resources set to featured (from the resource detail page)
     * @param benf\neo\elements\db\BlockQuery with \benf\neo\elements\Block
     * @return array of craft\elements\Entry
     */
    public function getFeaturedResources($cardsManuallySet)
    {

        $numberSelectedAlready = count($cardsManuallySet);
        if ($numberSelectedAlready >= 3) {
            return $cardsManuallySet;
        } else {
            $numberNeeded = 3 - $numberSelectedAlready;
        }

        $excludeIds = [];
        $selectedEntries = [];
        foreach ($cardsManuallySet as $selected) {
            if ($selected instanceof \benf\neo\elements\Block) {
                $cardMatrix = $selected->resourceCard;
                foreach ($cardMatrix->all() as $childBlock) {
                    $childBlockHandle = $childBlock->getType()->handle;
                    if ($childBlockHandle == 'chooseFromEntries') {
                        $sourceEntry = $childBlock->entry->one();
                        $selectedEntries[] = $sourceEntry;
                        $excludeIds[] = $sourceEntry->id;
                    }
                }
            } else {
                $selectedEntries[] = $selected;
            }
        }

        $criteria = Entry::find();
        $criteria->limit = $numberNeeded;
        $criteria->section = "resources";
        $criteria->featured = 1;
        if (count($excludeIds) > 0) {
            $criteria->id = 'and, not ' . implode(', not ', $excludeIds);
        }
        $criteria->order = 'RAND()';

        $featuredEntries = $criteria->all();
        $allEntries = array_merge($selectedEntries, $featuredEntries);

        return $allEntries;
    }

    /**
     * Get Related Resources
     * @param craft\elements\Entry $entry with type of resource
     * @return array of max 3x craft\elements\Entry
     */
    public function getRelatedResources($entry)
    {
        $relatedResources = [];

        $entryResourceAttrs = $entry->resourceAttributes;
        $entryResAttrIds = [];
        foreach ($entryResourceAttrs->all() as $entryResAttr) {
            $entryResAttrIds[] = $entryResAttr->ID;
        }

        $criteria = Entry::find();
        $criteria->limit = null;
        $criteria->section = "resources";
        $criteria->id(['not', $entry->ID]);
        $criteria->orderBy = 'postDate';

        //RESULT:
        //loop through and stop when have 3
        $allResources = $criteria->all();

        $entryRequiresLogin = $this->requiresLogin($entry);

        $entryResChannelId = $entry->resourceChannel->one()->id;

        foreach ($allResources as $resource) {
            // exclude those behind login if this page is not members only
            if (!$entryRequiresLogin && $this->requiresLogin($resource)) {
                continue;
            }

            // exclude those from different resource Channel
            if ($resource->resourceChannel->one()->id !== $entryResChannelId) {
                continue;
            }

            $resAttrs = $resource->resourceAttributes;
            $resAttrIds = [];
            foreach ($resAttrs->all() as $resAttr) {
                if (in_array($resAttr->ID, $entryResAttrIds)) {
                    $relatedResources[] = $resource;
                    break;
                }
            }

            if (count($relatedResources) == 3) {
                break;
            }
        }

        return $relatedResources;
    }

    /**
     * Get Resources Landing Page from a Resource Details Page
     * Matched by Resource Channel ID
     * @param craft\elements\Entry $entry
     * @return craft\elements\Entry
     */
    public function getResourcesLandingFromResDetail($entry)
    {
        $entryResChannelId = $entry->resourceChannel->one()->id;
        $criteria = Entry::find();
        $criteria->limit = null;
        $criteria->section = "pages";
        $criteria->type = 'resourceLanding';
        $resourceLandingPages = $criteria->all();
        foreach ($resourceLandingPages as $page) {
            $resourceChannelId = $page->resourceChannel->one()->id ?? null;
            if ($resourceChannelId == $entryResChannelId) {
                return $page;
            }
        }
        return false;
    }

    /**
     * Determines if an entry requires a login either because it has members only switch enabled
     * OR its a resource with a resource channel that is set to members only
     * @param craft\elements\Entry $entry
     * @return bool
     */
    public function requiresLogin($entry)
    {

        if (isset($entry->membersOnly) && $entry->membersOnly) {
            return true;
        }
        $resourceChannel = $entry->resourceChannel ?? null;
        if ($resourceChannel) {
			// echo "here:";
			// $this->dump($resourceChannel->title);
            return !$resourceChannel->one()->resourceChannelAccess;
        }
        return false;
    }


    public function userCanAccessThisSite()
    {
        // $this->dump(Craft::$app->getUser()->getIdentity());
    }

    /**
     * Get Resource Attributes split into the 4 types
     *
     * @param craft\elements\Entry $entry
     * @return array
     */
    private function getResourceAttributes($entry)
    {

        $attrs = [
            'resourceCategories' => [],
            'resourceTopics' => [],
            'resourceTypes' => [],
            'resourceAudiences' => [],
        ];

        foreach ($entry->resourceAttributes->all() as $key => $attr) {
            $attrs[$attr->section->handle][$attr->id]['title'] = $attr->title;
            $attrs[$attr->section->handle][$attr->id]['cardLabel'] = $attr->cardLabel ?? $attr->title;
            $attrs[$attr->section->handle][$attr->id]['id'] = $attr->id;
        }

        return $attrs;
    }

    public function getResourceTags($entry)
    {
        $tags = [
            'tagsUrl' => [],
            'topTags' => [],
            'bottomTags' => []
        ];
        $topTags = [];
        $bottomTags = [];
        $tagsUrl = $this->getResourcesLandingFromResDetail($entry)->url ?? null;

        $resourceAttrEntries = is_array($entry->resourceAttributes) ? $entry->resourceAttributes : [];
        if (count($resourceAttrEntries) > 0) {
            foreach ($resourceAttrEntries->all() as $resAttrEntry) {
                // $resAttrEntry->title;
                $resAttrHandle = $resAttrEntry->getSection()->handle;
                // TODO: Might be that same handle appears in different places on different sites
                switch ($resAttrHandle) {
                    case 'resourceAudiences':
                    case 'resourceTypes':
                        $topTags[$resAttrEntry->id]['title'] = $resAttrEntry->cardLabel ?? $resAttrEntry->title;
                        $topTags[$resAttrEntry->id]['id'] = $resAttrEntry->id;
                        break;
                    case 'resourceTopics':
                    case 'resourceCategories':
                        $bottomTags[$resAttrEntry->id]['title'] = $resAttrEntry->cardLabel ?? $resAttrEntry->title;
                        $bottomTags[$resAttrEntry->id]['id'] = $resAttrEntry->id;
                        break;
                    default:
                        break;
                }
            }
            $tags['topTags'] = $topTags;
            $tags['bottomTags'] = $bottomTags;
            $tags['tagsUrl'] = $tagsUrl;
        }
        return $tags;
    }

    /**
     * Get Resources - Used on VUE filtered resource landing pages
     * @param craft\elements\Entry -> ID $entryId
     * @return array of craft\elements\Entry
     */
    public function getResources($entryId)
    {
        // landing page
        $landingPage = Entry::find()
            ->id($entryId)
            ->one();

        // Build filters from Landing page
        $filters = $landingPage->filters;
        foreach ($filters->all() as $filter) {
            $options = [];
            foreach ($filter->filterOptions->all() as $option) {
                $optionItem = [
                    'label' => $option->title,
                    'section' => $option->section->handle,
                    'id' => $option->id
                ];
                $options[] = $optionItem;
            }
            $filterItem = [
                'label' => $filter->label,
                'placeholder' => $filter->placeholder,
                'id' => $filter->id,
                'options' => $options
            ];

            $resources['filters'][] = $filterItem;
        }

        // We only want resources from the resource Channel set on the landing page
        $landingResourceChannelId = $landingPage->resourceChannel->one()->id;

        // Resource Entries
        $criteria = Entry::find();
        $criteria->limit = null;
        $criteria->section = "resources";
        $resources['items'] = [];
        foreach ($criteria->all() as $entry) {
            if ($entry->resourceChannel->one()->id == $landingResourceChannelId) {

                // collection of just the ID's of the attr's so we can compare in Vue easily - not sure we can do this as we need to combine different filters with 'AND' logic
                // $attrIds = [];

                // separate res attr's into their respective categories
                $attrs = [
                    'resourceCategories' => [],
                    'resourceTopics' => [],
                    'resourceTypes' => [],
                    'resourceAudiences' => []
                ];

                foreach ($entry->resourceAttributes->all() as $key => $attr) {
                    $attrs[$attr->section->handle][$attr->id]['title'] = $attr->title;
                    $attrs[$attr->section->handle][$attr->id]['cardLabel'] = $attr->cardLabel ?? $attr->title;
                    $attrs[$attr->section->handle][$attr->id]['id'] = $attr->id;
                }

                $item = [
                    'id' => $entry->id,
                    'title' => $entry->title,
                    'url' => $entry->url,
                    'summary' => null,
                    'featured' => $entry->featured,
                    'resourceCategories' => $attrs['resourceCategories'],
                    'resourceTopics' => $attrs['resourceTopics'],
                    'resourceTypes' => $attrs['resourceTypes'],
                    'resourceAudiences' => $attrs['resourceAudiences'],
                    // 'allAttrIds' => $attrIds,
                    'postDate' => $entry->postDate ? $entry->postDate->format("j M Y") : null,
                    'thumbnailUrl' => null,
                    'type' => $entry->type,
                ];

                // get summary
                $summaryObj = @$entry['summary'];
                if ($summaryObj && $summaryObj instanceof \craft\redactor\FieldData) {
                    $item['summary'] = $summaryObj->__toString();
                }

                // get thumbnail
                $image = $entry->featuredImage->one();
                if ($image) {
                    $item['thumbnailUrl'] = YumpModule::$instance->yump->getImageCardThumbnail($image);
                }

                $resources['items'][] = $item;
            }
        }
		$resources['showTopTags'] = $landingPage->showTopTagsOnCards ?? true;

        return $resources;
    }

    /**
     * Get Login for IAS landing page
     *
     * @return craft\elements\Entry 
     */
    public function getLoginForIasPage()
    {

        $criteria = Entry::find();
        $criteria->limit = 1;
        $criteria->section = 'pages';
        $criteria->type = 'loginForIasLanding';
        if ($criteria->one()) {
            return $criteria->one();
        }

        return null;
    }

    public function getSpecialistEquipmentItems()
    {
        $criteria = Entry::find();
        $criteria->limit = null;
        $criteria->orderBy = "postDate desc";
        $criteria->section = "specialistEquipmentDetail";
        $items = [];
        foreach ($criteria->all() as $entry) {
            $item = [
                'id' => $entry->id,
                'title' => $entry->title,
                'url' => $entry->url,
                'summary' => null,
                'thumbnailUrl' => null,
                'available' => null,
                'equipmentType' => null,
                'tags' => array(),
                'availabilityClass' => 'card__tag--available',
                'type' => $entry->type,
            ];
            // get summary
            $summaryObj = @$entry['summary'];
            if ($summaryObj && $summaryObj instanceof \craft\redactor\FieldData) {
                $item['summary'] = $summaryObj->__toString();
            }

            // get thumbnail
            $image = $entry->featuredImage->one();
            if ($image) {
                $item['thumbnailUrl'] = YumpModule::$instance->yump->getSpecialistImageCardThumbnail($image, [], false);
            }
            // item's equipment type
            $equipmentTypes = @$entry['equipmentType']->all();
            $alltypes = [];
            foreach ($equipmentTypes  as $equipmentType) {
                $alltypes[] = [
                    'title' => $equipmentType->title,
                    'value' => $equipmentType->id,
                ];
            }
            $item['equipmentType'] = $alltypes;
            // item's availability
            $available = @$entry['available'];
            if ($available) {
                $item['available'] = 'Available';
                $item['availabilityClass'] = 'card__tag--available';
            } else {
                $item['available'] = 'Unavailable';
                $item['availabilityClass'] = 'card__tag--unavailable';
            }
            // get tags
            $searchTagFieldHandle = $this->getConfig('searchTagFieldHandle');
            $searchTagEntries = $searchTagFieldHandle ? [] : $entry[$searchTagFieldHandle]->all();
            if (!empty($searchTagEntries)) {
                foreach ($searchTagEntries->all() as $tag) {
                    $item['tags'][] = $tag['title'];
                }
            }
            $items[] = $item;
        }

        return $items;
    }

    public function getAllEquipmentTypes($entryHandle)
    {
        $types = Entry::find()
            ->section($entryHandle)
            ->orderBy('title ASC')
            ->all();
        $allTypes = [];
        foreach ($types as $type) {
            $allTypes[] = [
                'value' => $type->id,
                'label' => $type->title,
            ];
        }
        return $allTypes;
    }

    /**
     * Get the breadcrumb items, for a page type entry.
     *
     * @param  craft\elements\Entry $entry (assuming an entry in pages section, or can be used in any other structure type sections as well. This function doesn't work for channel type sections)
     * @param  boolean $includingHome whether we should include the home crumb or not. According to our designer, home crumb should only be included if Home is not present in the top menu.
     * @return array        array of the crumbs.
     */
    public function getPageEntryCrumbs($entry, $includingHome = true, $includingSelf = true)
    {
        $crumbs = [];

        // include home if required
        if ($includingHome) {
            $crumbs[] = [
                'text' => 'Home',
                'url' => '/',
            ];
        }

        // include ancestors
        $ancestors = $entry->getAncestors()->hideFromNavigation('not 1')->all();
        foreach ($ancestors as $ancestor) {
            $crumb = [
                'text' => $ancestor->title,
                'url' => $ancestor->getUrl(),
            ];
            if ($ancestor->getType()->handle == 'linkOnly') {
                $crumb['target'] = $ancestor->cta->getTarget();
            }
            $crumbs[] = $crumb;
        }

        if ($includingSelf) {
            // flag the parent
            if (!empty($crumbs)) {
                $crumbs[count($crumbs) - 1]['isParent'] = true;
            }

            // include current entry at the end.
            $crumbs[] = [
                'text' => $entry->title,
            ];
        }

        return $crumbs;
    }

    public function isElementQuery($object)
    {
        return $object instanceof \craft\elements\db\ElementQuery;
    }

    public function isEntryQuery($object)
    {
        return $object instanceof \craft\elements\db\EntryQuery;
    }

    public function isNeoBlockQuery($object)
    {
        return $object instanceof \benf\neo\elements\db\BlockQuery;
    }

    /**
     * Translate different $card types into the same card meta attributes.
     * @param  mixed $card
     * @param  string|false $forceCardType if we want to force the card type (usually it's 'image' or 'text-only', auto-detected if this is coming from a neo block). Mainly use this for situations when you need to manually include the cards template and set the cards attr to an ElementQuery (e.g. EntryQuery)
     * @param  array $imagerSettings if we need custom imagerSettings. Without it, we still have default settings defined in general config file (general.php)
     * @return array|null if not an Entry, nor a Block
     */
    public function translateCardBio($card, $forceCardType = false, $imagerSettings = array())
    {

        $heading = null;
        $summary = null;
        $image = null;
        $thumbnailUrl = null;
        $imageCaption = null;
        $imageAlt = null;
        $url = null;
        $tagsUrl = null;
        $topTags = [];
        $singleTag = "";
        $bottomTags = [];
        $postDate = null;
        $ctaText = $this->getConfig('defaultCardCtaText');
        $ctaTarget = null;
        $type = null;
        $cardType = $forceCardType ?: 'image';
        $availableClass = 'available';

        if ($card instanceof \craft\elements\Entry) {
            $type = $card->type;
            $heading = $card->title;
            $summary = @$card->summary;
			$handle = $card->getSection()->handle ?? null;
            $resourceAttrEntries = is_array($card->resourceAttributes) ? $card->resourceAttributes : [];
            if (count($resourceAttrEntries) > 0) {

                $tagsUrl = $this->getResourcesLandingFromResDetail($card)->url ?? null;

                // TODO: Use getResourceTags instead of this so we can handle it all in one place
                $resources['items'] = [];
                foreach ($resourceAttrEntries->all() as $resAttrEntry) {
                    // $resAttrEntry->title;
                    $resAttrHandle = $resAttrEntry->getSection()->handle;
                    switch ($resAttrHandle) {
                        case 'resourceAudiences':
                        case 'resourceTypes':
                            $topTags[$resAttrEntry->id]['title'] = $resAttrEntry->cardLabel ?? $resAttrEntry->title;
                            $topTags[$resAttrEntry->id]['id'] = $resAttrEntry->id;
                            break;
                        case 'resourceTopics':
                        case 'resourceCategories':
                            $bottomTags[$resAttrEntry->id]['title'] = $resAttrEntry->cardLabel ?? $resAttrEntry->title;
                            $bottomTags[$resAttrEntry->id]['id'] = $resAttrEntry->id;
                            break;
                        default:
                            break;
                    }
                }
            }
            if ($handle && $handle == 'specialistEquipmentDetail') {
                if ($card->available) {
                    $singleTag = 'Available';
                } else {
                    $singleTag = 'Unavailable';
                    $availableClass = 'unavailable';
                }
                $equipmentTypes = $card->equipmentType->all();
                foreach ($equipmentTypes as $equipmentType) {
                    $bottomTags[] = [
                        'title' => $equipmentType->title,
                        'value' => $equipmentType->id,
                    ];
                }
            }
            // set date for news
            if ($handle && $handle == 'news') {
                $postDate = $card->postDate ? $card->postDate->format("j M Y") : null;
            }
            if ($cardType == 'image') {
                $image = $card->featuredImage->one();
            }
            $url = $card->url;
        } else if ($card instanceof \benf\neo\elements\Block) {
            $neoBlockHandle = $card->getType()->handle ?? null;
            if ($neoBlockHandle == 'imageCard') {
                $cardType = $forceCardType ?: 'image';
                $cardMatrix = $card->imageCard;
            } else if ($neoBlockHandle == 'textOnlyCard') {
                $cardType = $forceCardType ?: 'text-only';
                $cardMatrix = $card->textOnlyCard;
            } else {
				$cardType = $forceCardType ?: 'image';
				$cardMatrix = $card->imageCard;
			}
            foreach ($cardMatrix->all() as $childBlock) {
                $childBlockHandle = $childBlock->getType()->handle;
                if ($childBlockHandle == 'chooseFromEntries') {
                    $sourceEntry = $childBlock->entry->one();
                    // re-use this function to get card bio from the entry
                    $cardBio = $this->translateCardBio($sourceEntry, $forceCardType, $imagerSettings);
                    $cardBio['cardType'] = $cardType; // overwrite card type
                    // update the ctaText if customCtaText is defined.
                    $customCtaText = trim($childBlock->customCtaText);
                    if (!empty($customCtaText)) {
                        $cardBio['ctaText'] = $customCtaText;
                    }
                    return $cardBio;
                } else if ($childBlockHandle == 'manualInput') {
                    $heading = $childBlock->heading;
                    $summary = $childBlock->summary;
                    if ($cardType == 'image') {
                        $image = $childBlock->image->one(); 
                    }
                    $linkField = $childBlock->linkTo;
                    if (!empty($linkField)) {
                        $url = $this->generateValidUrl($linkField->getUrl());
                        $ctaText = $linkField->getCustomText($ctaText);
                        $ctaTarget = $linkField->getTarget();
                    }
                }
            }
        } else {
            return;
        }

        if ($image) {
            $imageCaption = $image->imageCaption;
            $thumbnailUrl = $this->getImageCardThumbnail($image, $imagerSettings);
			if ($card instanceof \craft\elements\Entry){
				$handle = $card->getSection()->handle;
			} else {
				$handle = null;
			}
            if ($handle && $handle == 'specialistEquipmentDetail') {
                $thumbnailUrl = $this->getSpecialistImageCardThumbnail($image, $imagerSettings);
            }
            $imageAlt = $image->title ?? '';
        }

        return [
            'heading' => $heading,
            'summary' => $summary,
            'type' => $type,
            // 'image' => $image, // we don't really need it, but return it anyway just in case.
            'thumbnailUrl' => $thumbnailUrl,
            'imageCaption' => $imageCaption,
            'imageAlt' => $imageAlt,
            'tagsUrl' => $tagsUrl,
            'topTags' => $topTags,
            'singleTag' => $singleTag,
            'bottomTags' => $bottomTags,
            'url' => $url,
            'ctaText' => $ctaText,
            'ctaTarget' => $ctaTarget,
            'postDate' => $postDate,
            'cardType' => $cardType,
            'availableClass' => $availableClass,
        ];
    }

    public function getImageCardThumbnail($image, $imagerSettings = array())
    {
        if ($image) {
            $defaultImagerSettings = $this->getConfig('imageCardImagerDefaultSettings') ?? [];
            $imagerSettings = array_merge_recursive($defaultImagerSettings, $imagerSettings);
            return $this->getThumbnail($image, $imagerSettings);
        }
    }
    public function getSpecialistImageCardThumbnail($image, $imagerSettings = array())
    {
        if ($image) {
            $defaultImagerSettings = $this->getConfig('imageCardspecialistDefaultSettings') ?? [];
            $imagerSettings = array_merge_recursive($defaultImagerSettings, $imagerSettings);
            return $this->getThumbnail($image, $imagerSettings);
        }
    }

    public function getNav()
    {

        if (!$this->_cachedNavPhpArray) {
//             Craft::info("get fresh php array:nav", __METHOD__);

            $includeHome = $this->getConfig('navShouldIncludeHomepage');
            $adapter = new YumpNavAdapter(
                ['includeHome' => $includeHome ? true : false] // whether we should include homepage in the nav list. If this doesn't work, you will probably need to clear cache, because it's only used in getFreshContent() function.
            );

            $this->_cachedNavPhpArray = $adapter->getContent();
        }

//         else {
//             Craft::info("get CACHED php array:nav", __METHOD__);
//         }

        return $this->_cachedNavPhpArray;
    }

    public function forceUpdateNav()
    {
        try {
            $includeHome = $this->getConfig('navShouldIncludeHomepage');
            $adapter = new YumpNavAdapter(
                ['includeHome' => $includeHome ? true : false] // whether we should include homepage in the nav list.
            );
            if ($adapter->forceUpdateCache()) {
                return array(
                    'success' => true,
                );
            } else {
                throw new \Exception("Setting cache was not successful. Please refer to yump.log for details.");
            }
        } catch (\Exception $e) {
            return array(
                "error" => "Failed to update nav cache. Error message: " . $e->getMessage(),
            );
        }
    }

    public function findItemInNav($navItems, $entryId)
    {
        foreach ($navItems as $navItem) {
            if ($navItem['id'] == $entryId) {
                return $navItem;
            } else {
                if (isset($navItem['children'])) {
                    $maybeFoundItem = $this->findItemInNav($navItem['children'], $entryId);
                    if ($maybeFoundItem) {
                        return $maybeFoundItem;
                    }
                }
            }
        }

        return false;
    }

    public function findParentItemInNav($navItems, $entryId)
    {
        foreach ($navItems as $navItem) {
            if (in_array($entryId, $navItem['childrenIds'])) {
                return $navItem;
            } else {
                if (isset($navItem['children'])) {
                    $maybeFoundItem = $this->findParentItemInNav($navItem['children'], $entryId);
                    if ($maybeFoundItem) {
                        return $maybeFoundItem;
                    }
                }
            }
        }

        return false;
    }

    public function yump_json_decode($content, $exceptionMessage = null, $compressException = false)
    {
        $contentInArray = json_decode($content, true);
        $jsonLastError = json_last_error();
        if ($jsonLastError === JSON_ERROR_NONE) {
            return $contentInArray;
        } else {
            if (!$compressException) {
                $exceptionMessage = $exceptionMessage ?? "Failed to json_decode the data";
                throw new \Exception($exceptionMessage . ' Json decode error was: ' . $jsonLastError . ".");
            }
            Craft::error($exceptionMessage . ' Json decode error was: ' . $jsonLastError . ".", __METHOD__);
        }
    }

    /**
     * It is possible that when you do $entry->id, or entry.id in Twig, you get the draft's element ID, which might not be the entry id we are looking for. This function is to get draft's sourceId (which is the active entry's ID) if it detects it is a draft
     * @param  Entry $entry
     * @return int|null     [description]
     */
    public function getEntryId(Entry $entry) {
        if($entry->getIsDraft()) {
            return $entry->getCanonicalId();
        }
        return $entry->id;
    }
    /**
     *
     * @param  [array - entries] $featuredPosts [entries selected by the admin to feature]
     * @param  [string] $contentFeed [matches the entryType handle]
     * @return [array] [array of featured posts and main set of posts]
     */
    public function getLandingPageEntries($featuredPosts, $contentFeed)
    {
        $posts = [];
        $featuredPostIDs = [];

        $numPosts = sizeof($featuredPosts);
        if ($numPosts < 2) {
            //If less than 2, get the latest posts to supplement the featured set of articles with the latest entries
            $exclude = '';

            // $criteria = Entry::find()->section($contentFeed)->orderBy('postDate desc');
            // if($numPosts == 1) {
            //     $exclude = $featuredPosts[0]['id'];
            //     $criteria->id(['not', $exclude])
            // }

            //Exclude the one that the admin has already specified (if they have specified one)
            if ($numPosts == 1) {
                $exclude = $featuredPosts[0]['id'];
                $supplementaryPosts = Entry::find()
                    ->section($contentFeed)
                    ->orderBy('postDate desc')
                    ->limit(2 - $numPosts)
                    ->id(['not', $exclude])
                    ->all();
            } else {
                $supplementaryPosts = Entry::find()
                    ->section($contentFeed)
                    ->orderBy('postDate desc')
                    ->limit(2 - $numPosts)
                    ->all();
            }
            $posts['featured'] = array_merge($featuredPosts, $supplementaryPosts);
        } else {
            //admin has set two themselves, use those for the featured posts
            $posts['featured'] = $featuredPosts;
        }

        //Get ID's of the featured posts so we can use those ID's to exclude them from the main post list
        foreach ($posts['featured'] as $featured) {
            $featuredPostIDs[] = $featured['id'];
        }

        $posts['main'] = Entry::find()
            ->section($contentFeed)
            ->orderBy('postDate desc')
            ->id('and, not ' . implode(', not ', $featuredPostIDs))
            ->limit(6);

        return $posts;
    }

    /**
     * Get all entry types of the website. Has the ability to return the data as array
     * @param  boolean $toArray     true: return each entry type as array. false: return each as EntryType object
     * @param  boolean $handleAsKey only works if $toArray is true. Determine whether id or handle is used as the array key
     * @return [type]               [description]
     */
    public function getAllEntryTypes($toArray = false, $handleAsKey = false)
    {
        $siteEntryTypes = Craft::$app->sections->getAllEntryTypes();

        if ($toArray) {
            $result = [];

            foreach ($siteEntryTypes as $key => $entryType) {
                $obj = [
                    'id' => $entryType->id,
                    'name' => $entryType->name,
                    'sectionId' => $entryType->sectionId,
                    'handle' => $entryType->handle,
                    'fieldLayoutId' => $entryType->fieldLayoutId,
                    'sortOrder' => $entryType->sortOrder,
                    'hasTitleField' => $entryType->hasTitleField,
                    'titleFormat' => $entryType->titleFormat,
                    'uid' => $entryType->uid,
                ];

                if ($handleAsKey) {
                    $result[$obj['handle']] = $obj;
                } else {
                    $result[$obj['id']] = $obj;
                }
            }
            return $result;
        } else {
            return $siteEntryTypes;
        }
    }

    /**
     * Get the list of types from 'searchFiltersMapping' in general.php
     * @param  [type] $type 'entryTypes', or 'assetVolume'
     * @return [array] array of ids
     */
    private function _getIdsForSearch($type)
    {
        $searchFiltersMapping = $this->getConfig('searchFiltersMapping');

        $ids = [];
        if ($searchFiltersMapping) {
            foreach ($searchFiltersMapping as $key => $settings) {
                if ($settings['type'] == $type) {
                    $ids = array_merge($ids, $settings['ids']);
                }
            }
        }

        return $ids;
    }

    /**
     * Get the breadcrumb of an entry
     * @param  [type] $entry [description]
     * @return [array]        the breadcrumb structure
     */
    public function getBreadcrumb($entry, $addSelf = true)
    {

        $crumbs = [];

        // Shall we include Home crumb? if we don't have homepage in the nav list, then the breadcrumb should include Home crumb; otherwise it shouldn't.
        $breadcrumbShouldIncludeHome = !$this->getConfig('navShouldIncludeHomepage');
        if ($breadcrumbShouldIncludeHome) {
            $crumbs[] = [
                'text' => 'Home',
                'url' => '/',
            ];
        }

        $generalSettings = \Craft::$app->getGlobals()->getSetByHandle('generalSettings');
        $sectionHandle = $entry->getSection()->handle;

        // get crumbs based on the section
        $nav = $this->getNav();
        switch ($sectionHandle) {

                // if this is under 'pages' entry, simply get the path from cached nav
            case 'pages':
                $crumbs = $this->buildFamilyCrumbs($crumbs, $nav, $entry['id']);
                $crumbs[array_key_last($crumbs)]['isParent'] = true;
                break;

                // if this is in a separate section (e.g. news, as a channel). Its parent is a landing page under 'pages' - so get the breadcrumb of the landing page first. Reminder to set the landing page ID in general.php
            case 'news':
                $newsLandingPage = $this->getNewsLandingPageForSite();
                if ($newsLandingPage) {
                    $crumbs = $this->buildFamilyCrumbs($crumbs, $nav, $this->getConfig('newsLandingPageId'));
                    $crumbs[] = [
                        'text' => $newsLandingPage['title'],
                        'url' => $newsLandingPage['url'],
                        'isParent' => true,
                    ];
                }
                break;

                // ... add more sections similar to 'news' here

            default:
                // do nothing, so "Home" will be the only crumb before self
                break;
        }

        // shall we add self?
        if ($addSelf) {
            $crumbs[] = [
                'text' => $entry['title'],
            ];
        }

        return $crumbs;
    }

    public function buildFamilyCrumbs($crumbs, $branch, $entryId)
    {
        foreach ($branch as $page) {
            if (!empty($page['descendantsIds']) and in_array($entryId, $page['descendantsIds'])) {

                // if the target page is a descendant, add the current page to the crumbs anyway.
                $crumbs[] = [
                    'text' => $page['title'],
                    'url' => $page['url'],
                ];

                // if the target page is not the children, then do the same search in the children recursively
                if (!in_array($entryId, $page['childrenIds']) and !empty($page['children'])) {
                    $crumbs = $this->buildFamilyCrumbs($crumbs, $page['children'], $entryId);
                }
            }
        }

        return $crumbs;
    }

    private function _getFilterKey($element)
    {
        $searchFiltersMapping = $this->getConfig('searchFiltersMapping');
        foreach ($searchFiltersMapping as $key => $settings) {
            if ($element instanceof Entry) {
                if ($settings['type'] == 'entryType' and in_array($element['typeId'], $settings['ids'])) {
                    return $key;
                }
            } else if ($element instanceof Asset) {
                if ($settings['type'] == 'assetVolume' and in_array($element['volumeId'], $settings['ids'])) {
                    return $key;
                }
            }
        }
    }

    private function _buildEntryDataForSearch($entry, $query)
    {
        $result = [
            'id' => $entry['id'],
            'score' => @$entry['searchScore'],
            'title' => $entry['title'],
            'url' => $entry['url'],
            'type' => 'entryType',
            'typeId' => $entry['typeId'],
            'filterKey' => $this->_getFilterKey($entry),
            'summary' => null,
            'tags' => array(),
            'breadcrumb' => $this->getBreadcrumb($entry),
            'highlightedTitle' => $this->searchExcerpt($entry['title'], $query),
        ];

        // get tags
        $searchTagFieldHandle = $this->getConfig('searchTagFieldHandle');
        if ($searchTagFieldHandle && !empty($entry[$searchTagFieldHandle])) {
            foreach ($entry[$searchTagFieldHandle] as $tag) {
                $result['tags'][] = $tag['title'];
            }
        }

        // get summary
        $summaryObj = @$entry['summary'];
        if ($summaryObj && $summaryObj instanceof \craft\redactor\FieldData) {
            $result['summary'] = $summaryObj->__toString();
            $result['highlightedSummary'] = $this->searchExcerpt($result['summary'], $query);
        }

        return $result;
    }

    private function _buildAssetDataForSearch($asset, $query)
    {
        $result = [
            'id' => $asset['id'],
            'score' => @$asset['searchScore'],
            'url' => $asset['url'],
            'title' => $asset['title'],
            'type' => 'assetVolume',
            'typeId' => $asset['volumeId'],
            'filterKey' => $this->_getFilterKey($asset),
            'summary' => null,
            'tags' => array(),
            'highlightedTitle' => $this->searchExcerpt($asset['title'], $query),
            'fileExtension' => $asset->getExtension(),
        ];

        // get tags
        $searchTagFieldHandle = $this->getConfig('searchTagFieldHandle');
        if ($searchTagFieldHandle && !empty($asset[$searchTagFieldHandle])) {
            foreach ($asset[$searchTagFieldHandle] as $tag) {
                $result['tags'][] = $tag['title'];
            }
        }

        // get summary
        $summaryObj = @$asset['summary'];
        if ($summaryObj && $summaryObj instanceof \craft\redactor\FieldData) {
            $result['summary'] = $summaryObj->__toString();
            $result['highlightedSummary'] = $this->searchExcerpt($result['summary'], $query);
        }

        return $result;
    }

    public function searchResults($query)
    {
        $results = [];
        $searchTagFieldHandle = $this->getConfig('searchTagFieldHandle');

        if (!empty($query) and !empty(trim($query))) {
            $entries = null;
            $searchableEntryTypeIds = $this->_getIdsForSearch('entryType');
            if (!empty($searchableEntryTypeIds)) {
                $criteria = Entry::find();
                $criteria->search = trim($query);
                $criteria->limit = null;
                $criteria->orderBy = 'score';
                $criteria->typeId = $searchableEntryTypeIds;
                if ($searchTagFieldHandle) {
                    $criteria->with = [$searchTagFieldHandle];
                }

                $entries = $criteria->all();

                foreach ($entries as $entry) {
					// $this->dump($this->currentUserCanAccessEntry($entry), false);
					if($this->currentUserCanAccessEntry($entry)){
						$results[] = $this->_buildEntryDataForSearch($entry, $query);
					}
                }
            }

            $assets = null;
            $searchableAssetVolumeIds = $this->_getIdsForSearch('assetVolume');
            if (!empty($searchableAssetVolumeIds)) {
                $criteria = Asset::find();
                $criteria->search = trim($query);
                $criteria->limit = null;
                $criteria->orderBy = 'score';
                $criteria->volumeId = $searchableAssetVolumeIds;
                if ($searchTagFieldHandle) {
                    $criteria->with = [$searchTagFieldHandle];
                }

                $assets = $criteria->all();

                foreach ($assets as $asset) {
                    $results[] = $this->_buildAssetDataForSearch($asset, $query);
                }
            }

            return $results;
        }
    }

    public function sanitisePhoneNumber($rawInput)
    {
        return preg_replace('/[^+\-0-9]/', '', $rawInput);
    }

    /**
     * Convert a tag or category field value into an array
     * @param  [Tag or Category value]  $query          [description]
     * @param  boolean $includeIdAsKey [description]
     * @return [array]                  [description]
     */
    public function getTagsOrCategoriesAsArray($query, $includeIdAsKey = false)
    {
        $result = [];
        if ($query) {
            foreach ($query->all() as $tag) {
                if ($includeIdAsKey) {
                    $result[$tag->id] = $tag->title;
                } else {
                    $result[] = $tag->title;
                }
            }
        }

        return $result;
    }

    /**
     * Get current user
     * @param [bool] $returnService
     * @return \craft\web\User if $returnService is true, otherwise (by default), \craft\element\User
     */
    public function getCurrentUser($returnService = false)
    {
        if ($returnService) {
            return Craft::$app->getUser();
        } else {
            return Craft::$app->getUser()->getIdentity();
        }
    }

    /**
     * Checks whether the current user is part of a user group that can access the folder which a given asset is in
     *
     * @param craft\elements\Asset $asset
     * @param \craft\web\User $currentUser
     * @return bool
     */
    public function currentUserCanAccessAsset($asset)
    {
        $currentUser = $this->getCurrentUser();
        // 6 - IDFM Protected Folder -> 2 - IDFM Member GRoup
        // 7 - NSW Protected -> 3 - NSW Member Group
        // 8 - QLD Protected -> 4 - QLD Member Group
        switch ($asset->volumeId) {
            case 6: // IDFM
                if (!$currentUser) {
                    return false;
                } else {
                    return $currentUser->isInGroup(2);
                }
                break;

            case 7: // NSW
                if (!$currentUser) {
                    return false;
                } else {
                    return $currentUser->isInGroup(3);
                }
                break;

            case 8: // QLD
                if (!$currentUser) {
                    return false;
                } else {
                    return $currentUser->isInGroup(4);
                }
                break;

                // not in a protected folder so user can access
            default:
                return true;
                break;
        }

        return true;
    }

    /**
     * Get Site ID from Asset
     *
     * @param \craft\elements\asset $asset
     * @return int
     */
    public function siteIdFromAsset($asset)
    {
        switch ($asset->volumeId) {
            case 6: // IDFM
                return 1;
                break;

            case 7: // NSW
                return 2;
                break;

            case 8: // QLD
                return 3;
                break;

                // not in a protected folder so user can access
            default:
                return 1;
                break;
        }
        return 1;
    }

    /**
     * Checks whether the current user can access an entry
     *
     * @param \craft\elements\entry $entry
     * @return bool
     */
    public function currentUserCanAccessEntry($entry)
    {
        // 1 - IDFM Site -> 2 - IDFM Member Group
        // 2 - NSW Site -> 3 - NSW Member Group
        // 3 - QLD Site -> 4 - QLD Member Group

        if (!$this->requiresLogin($entry)) {
            return true;
        }

        $currentUser = $this->getCurrentUser();

        // not logged in at all
        if (!$currentUser) {
            return false;
        }

        // can access the backend so return true
        if ($currentUser->can('accessCp')) {
            return true;
        };

        $siteIdOfEntry = $entry->siteId;

        //logged in - check if in right group
        switch ($siteIdOfEntry) {
            case 1: // IDFM
                return $currentUser->isInGroup(2);
                break;

            case 2: // NSW
                return $currentUser->isInGroup(3);
                break;

            case 3: // QLD
                return $currentUser->isInGroup(4);
                break;

                // should only apply if another site is created so default to true for that
            default:
                return true;
                break;
        }

        return false;
    }

    /**
     * @param $text
     * @return mixed|string
     */
    public function createJsFriendlySlug($text)
    {
        $slug = $this->createSlug($text);
        $slug = str_replace(".", "", $slug); // js doesn't allow . in id, so we remove it
        $slug = "id-" . $slug; //js does not allow id start with number, so we add a text in begin
        return $slug;
    }

    /**
     * Create Slug
     *
     * @param string text
     * @return string slug
     */
    public function createSlug($text)
    {
        if ($text == null) {
            $text = "";
        }

        return ElementHelper::normalizeSlug($text);
    }

    /**
     * Anchor Links - Generate an array of anchor links for the in-page navigation
     *
     * @param $entry
     * @return array Anchor Links
     */
    public function getAnchorLinks($entry)
    {

        $anchor_links = [];
        if ($entry instanceof \craft\elements\Entry) {
            $blocks = $entry['body']->all();
        } else {
            $blocks = null;
        }

        if ($blocks) {
            foreach ($blocks as $block) {
                foreach ($block->children->all() as $childBlock) {
                    if ($childBlock->type == 'anchorPoint') {
                        $anchorLink = $this->createJsFriendlySlug($childBlock->label);
                        // echo $anchorLink;
                        $anchor_links[] = ['link' => $anchorLink, 'label' => $childBlock->label];
                    } elseif ($childBlock->type == 'form' && $childBlock->heading) {
                        $anchorLink = $this->createSlug($childBlock->heading);
                        $anchor_links[] = ['link' => $anchorLink, 'label' => $childBlock->heading];
                    };
                }
            }
        }

        if (count($anchor_links) == 0) {
            return false;
        }

        return $anchor_links;
    }


	public function getContactPageForSite() {
		$siteHandle = Craft::$app->getSites()->currentSite->handle;
		$contactPageIds = $this->getConfig('contactPageIds', 'custom');
        if($contactPageIds){
            $contactPageId = $contactPageIds[$siteHandle];
            $criteria = Entry::find();
            $criteria->id = $contactPageId;
            $contactPage = $criteria->one() ?? null;
            return $contactPage;
        }
	}

    public function getNewsLandingPageForSite()
    {
        $siteHandle = Craft::$app->getSites()->currentSite->handle;
		$newsLandingPageIds = $this->getConfig('newsLandingPageIds');
		$newsLandingPageId = $newsLandingPageIds[$siteHandle];
		$criteria = Entry::find();
		$criteria->id = $newsLandingPageId;
		$newsLandingPage = $criteria->one() ?? null;

        return $newsLandingPage;
    }

    public function getNewsItems()
    {
        $criteria = Entry::find();
        $criteria->limit = null;
        $criteria->section = "news";
        $newsLandingPage = $this->getNewsLandingPageForSite();
        $featuredArticles = $newsLandingPage->featuredArticles->all();
        $excludeIds = [];
        foreach ($featuredArticles as $article) {
            $excludeIds[] = $article->id;
        }

        $criteria->id = 'and, not ' . implode(', not ', $excludeIds);

        $items = [];
        foreach ($criteria->all() as $entry) {
            $item = [
                'id' => $entry->id,
                'title' => $entry->title,
                'url' => $entry->url,
                'summary' => null,
                'tags' => YumpModule::$instance->yump->getTagsOrCategoriesAsArray($entry->tags),
                'types' => YumpModule::$instance->yump->getTagsOrCategoriesAsArray($entry->newsCategory, true),
                'postDate' => $entry->postDate ? $entry->postDate->format("j M Y") : null,
                'thumbnailUrl' => null,
                'type' => $entry->type,
            ];

            // get summary
            $summaryObj = @$entry['summary'];
            if ($summaryObj && $summaryObj instanceof \craft\redactor\FieldData) {
                $item['summary'] = $summaryObj->__toString();
            }

            // get thumbnail
            $image = $entry->featuredImage->one();
            if ($image) {
                $item['thumbnailUrl'] = YumpModule::$instance->yump->getImageCardThumbnail($image);
            }

            $items[] = $item;
        }

        return $items;
    }

    public function getNewsTypes()
    {
        $criteria = Category::find();
        $criteria->limit = null;
        $criteria->group = "newsCategories";

        $items = [];
        foreach ($criteria->all() as $category) {
            $items[] = [
                'value' => $category->id,
                'label' => $category->title,
            ];
        }

        return $items;
    }

    /**
     * Queue the job to auto flush the nav cache
     * @param $priority Represents the priority of the job. The lower the higher priority. In this case, we want it to happen before Blitz cache warming, so we give it a very high priority, which is 1 by default.
     * @return [type] [description]
     */
    public function queueNavRefresherJob($priority = 1)
    {
        $jobId = Queue::push(new NavRefresher(), $priority);
        if ($jobId) {
            Craft::info("Successfully queued NavRefresher job, with ID: $jobId", __METHOD__);
        } else {
            Craft::error("Failed to queue NavRefresher job. Please check queue.log for details.", __METHOD__);
        }
    }

    public function sanitiseOrganisationId($orgId)
    {
        return preg_replace("/[^A-Za-z0-9 ]/", '', $orgId);
    }
}
