<?php
/**
 * example module for Craft CMS 3.x
 *
 * Just an example to show how to create a new module and extend a few things from the yumpmodule.
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2019 Yump
 */

namespace modules\examplemodule\variables;

use modules\examplemodule\ExampleModule;

use Craft;

/**
 * example Variable
 *
 * Craft allows modules to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.exampleModule }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Yump
 * @package   ExampleModule
 * @since     1.0.0
 */
class ExampleModuleVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Yump Special:
     * If function in this class not found, go find in in service class
     */
    function __call($method, $arguments) {
        return call_user_func_array(array(ExampleModule::$instance->example, $method), $arguments);
    }

    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.example.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.example.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */
    public function exampleVariable($optional = null)
    {
        $result = "And away we go to the Twig template...";
        if ($optional) {
            $result = "I'm feeling optional today...";
        }
        return $result;
    }
}
