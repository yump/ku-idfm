<?php
/**
 * To use this class:
 *
 * Get cached content (also getFreshContent and set new cache if the cache does not exist / expired):
 * 1. At the beginning of the class which need to use this cache: use modules\examplemodule\gears\cache\adapters\Events as EventsCacheAdapter;
 * 2. In a function of the class, do:
 * 	$adapter = new EventsCacheAdapter(); // or directly: $adapter = new \modules\examplemodule\gears\cache\adapters\Events(); if you don't want to do Step 1.
 * 	$content = $adapter->getContent();
 *
 * Force update cache: $adapter->forceUpdateCache();
 * 1. When a change which can affect the cached content happens. E.g. when an event entry is updated / added in the CMS, then we will need to have an event listener listening for the event entry saved event. See example in YumpModule.php for how it listen to 'pages' entry saved event.
 * 2. To make things easier, can also do a Cron job which force update cache every 15 minutes for example.
 * 
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2019 Yump
 */

namespace modules\examplemodule\gears\cache\adapters;

use modules\yumpmodule\YumpModule;
use modules\yumpmodule\gears\Cache as CacheGears;

class Events extends CacheGears
{
	/**
	 * In case we need certain custom settings for getFreshContent() method
	 * @var [type]
	 */
	private $_settings;

	public function __construct($settings = array()) {
		$cacheKey = 'example-events-cache';

		parent::__construct($cacheKey
			, CacheGears::CACHE_METHOD_YUMP // By default it uses Yump cache. If you want to use Craft cache instead, do it here
			, true // returnAsArray
		);

		// // set the cache to public folder, so front end code can access it directly.
		// $this->setYumpCacheOptions([
		// 	'usePublicFolder' => true,
		// ]);

		// $this->setReturnedAsArray(true);

		$this->_settings = $settings;
	}

	public function getFreshContent() {

		// $this->_settings, or any custom params passed into this constructor and saved can be used here to build the fresh content.
		
		// dummy array for testing
		$arr = [
			array(
				'id' => 1,
				'name' => 'Event 1',
			),
			array(
				'id' => 2,
				'name' => 'Event 2',
			),
			array(
				'id' => 3,
				'name' => 'Event 3',
			),
		];

		return json_encode($arr
			, JSON_PRETTY_PRINT // for debugging purpose only, so we can have the JSON more human readable.
		);
	}

}