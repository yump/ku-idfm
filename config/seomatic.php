<?php
/**
 * https://nystudio107.com/docs/seomatic/Configuring.html#multi-environment-config-settings
 */

return [
    // Global settings
    '*' => [
        
    ],

    // Dev environment settings
    'dev' => [
        'environment' => 'local',
    ],

	// Staging environment settings
    'staging' => [
        'environment' => 'live', // we want to allow GTM to run - we use a statging container (variable from .env to send data to)
    ],

    // Production environment settings
    'production' => [
        // The server environment, either `live`, `staging`, or `local`
        'environment' => 'live',
    ],
];
