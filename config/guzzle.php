<?php

return [

	'*' => [
		'verify' => true, // this is Guzzle / Craft default setting anyway.
	],

	'dev' => [
		'verify' => false, // or can be a specific path to cacert.pem in your local machine, e.g. "C:/wamp/bin/php/cacert.pem". However, each dev may have different path so probably not good to specify it and leave it in the Git repo. But not sure if this would raise any security concerns. See: https://craftcms.com/docs/3.x/config/#guzzle-config, https://docs.guzzlephp.org/en/latest/request-options.html#verify
	],

];