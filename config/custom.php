<?php
/**
 * Custom Configuration
 *
 * For Craft config settings, see: general.php
 *
 * Settings here can be accessed by: Craft::$app->config->custom->{mycustomsetting}
 */

return [
    // Global settings
    '*' => [

        // 'defaultImageQuality' => 100,
        'yumpCurlCacheFolder' => CRAFT_BASE_PATH . '/yump/private/cache/curl',
        'enableLoginWallOnDocs' => true,
        'enableFacebookCommentsOnDocs' => false,
        // cache
        'carerSupportGroupCacheKey' => 'support-group',
        'cardBannerCtaText' => 'Read more',
        'eventCardCtaText' => 'View event',
        'defaultCardCtaText' => 'Learn more',
        'imageCardPlaceholderUrl' => false,
        'controllerActionsPassword' => 'QUpX67EkiETU7Rg',
        // if path provided, we will use the placeholder image when card image is missing. The placeholder image's dimension should follow the 'imageCardImagerDefaultSettings'
                // Card default settings
        'imageCardImagerDefaultSettings' => array(
            'width' => 1076,
            'ratio' => 16 / 9,
        ),
        // Card default settings for special equipment library
        'imageCardspecialistDefaultSettings' => array(
            'width' => 1221,
            'ratio' => 1 / 1,
        ),
        'newsLandingPageQld' => '159807',
        // cache
        'siteNavigationCacheKey' => 'site-navigation',
        'siteMenuCacheKey' => 'site-menu',
        'navShouldIncludeHomepage' => true, // if set to true, then the cached navigation content will include homepage at the front of the navItems list, when running the getNav() function under YumpModuleService. Please note, if we don't have homepage in the nav list, then the breadcrumb should include Home crumb; otherwise it shouldn't.

        // search filters mapping (manually put in the IDs by checking the CMS)
        'searchFiltersMapping' => array(
            'pages' => array(
                'type' => 'entryType',
                'ids' => [2, 3, 4, 11, 14, 25],
            ),
            'news' => array(
                'type' => 'entryType',
                'ids' => [5],
            ),
            'resources' => array(
                'type' => 'entryType',
                'ids' => [21],
            ),
            'specialistEquipmentDetail' => array(
                'type' => 'entryType',
                'ids' => [26],
            ),
        ),
        // what's the handle for the tags field, for entries and documents.
        'searchTagFieldHandle' => 'tags',
        //'1' is the id of block articleWidthContent in neoblocktypes table
        'blockTypeIdWithSidebar' => 1,
         //put the ids of sections you want to hide any fullWidthContent and boxWidthContent before ArticleWidthContent
        //check craft table `entryTypes` for id.
        'entryTypeIdsForceArticleContentFirst' => [2, 5, 12],
        // social media
        // define which social media links are shown. These URLs must be defined in SEOMatic under Social Media -> 'Same as Links' in order to display.
        // 'activeSocialMediaIcons' => [
        //     'twitter',
        //     'facebook',
        //     'youtube',
        //     'linkedin',
        //     'instagram',
        // ],
        'activeSocialMediaIcons' => array(
            array(
                'handle' => 'facebook',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'facebook-square',
            ),
            array(
                'handle' => 'twitter',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'twitter-square',
            ),
            array(
                'handle' => 'youtube',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'youtube-square',
            ),
            array(
                'handle' => 'instagram',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'instagram',
            ),
            array(
                'handle' => 'linkedin',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'linkedin',
            ),
        ),
        // custom script permission control
        'controllerActionsAllowedIps' => array(
            '127.0.0.1',
            '43.241.54.230', // staging.yump.com.au (new server)
        ),
        'newsLandingPageIds' => array(
            'nswActInclusionAgency' => 14794,
            'inclusionSupportQld' => 159807,
        ),

		'contactPageIds' => array(
			'default' => '11483',
            'nswActInclusionAgency' => 13751,
            'inclusionSupportQld' => 13713,
        ),

        'thumbnailPlaceholderUrl' => 'https://via.placeholder.com/300x200',

        'imageTransformEngine' => getenv('IMAGE_TRANSFORM_ENGINE') ?: 'imager',

    ],

];
