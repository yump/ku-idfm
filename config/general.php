<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 *
 * @see \craft\config\GeneralConfig
 */

return [
    // Global settings
    '*' => [
        // Default Week Start Day (0 = Sunday, 1 = Monday...)
        'defaultWeekStartDay' => 1,

        // Whether generated URLs should omit "index.php"
        'omitScriptNameInUrls' => true,

        // error page templates will be located under template root with '_' as prefix
        'errorTemplatePrefix' => "_errors/",

        // Control Panel trigger word
        'cpTrigger' => 'admin',

        // Whether Craft should allow system and plugin updates in the Control Panel, and plugin installation from the Plugin Store.
        'allowUpdates' => false,

        // The secure key Craft will use for hashing and encrypting data
        'securityKey' => getenv('SECURITY_KEY'),

        // Enable CSRF Protection (recommended)
        'enableCsrfProtection' => true,

        // samesite cookie value for security purposes. Craft's default is null, which means missing, but modern browsers default it to Lax anyway. We just explicitly set it to avoid questions from pen testers.
        // Ref: https://craftcms.com/docs/3.x/config/config-settings.html#samesitecookievalue
        'sameSiteCookieValue' => 'Lax',

        // For security reasons, set this to true to make enumerate users difficult. 
        // Ref: https://craftcms.com/docs/3.x/config/config-settings.html#preventuserenumeration
        'preventUserEnumeration' => true,

        // login path
        'loginPath' => 'admin/login',
        // 'errorTemplatePrefix' => "_errors/",

    

        'aliases' => [
            '@idfmSiteUrl' => getenv('IDFM_BASE_SITE_URL'),
            '@nswSiteUrl' => getenv('NSW_BASE_SITE_URL'),
			'@qldSiteUrl' => getenv('QLD_BASE_SITE_URL'),
            '@idfmSiteHost' => getenv('IDFM_BASE_SITE_HOST'),
            '@nswSiteHost' => getenv('NSW_BASE_SITE_HOST'),
			'@qldSiteHost' => getenv('QLD_BASE_SITE_HOST'),
            '@webroot' => CRAFT_BASE_PATH
        ],



        

      


        'setPasswordPath' => 'set-password',

        'setPasswordSuccessPath' => array(
            'en' => 'password-success',
        ),

   



       
    ],

    // Dev environment settings
    'dev' => [
        // Dev Mode (see https://craftcms.com/guides/what-dev-mode-does)
        'devMode' => true,
        'allowUpdates' => true,
        'testToEmailAddress' => array(
            'devs@yump.com.au' => 'Yump Devs',
        ),
        'aliases' => [
            '@web' => getenv('PRIMARY_SITE_URL'),
        ],
    ],

    // Staging environment settings
    'staging' => [
        // Set this to `false` to prevent administrative changes from being made on staging
        'allowAdminChanges' => true,

        // Dev Mode (enabled when ?devMode=1 querystring is present)
        'devMode' => !empty($_GET['devMode']),
        'aliases' => [
            '@web' => getenv('PRIMARY_SITE_URL'),
        ],
    ],

    // Production environment settings
    'production' => [
        // Set this to `false` to prevent administrative changes from being made on production
        'allowAdminChanges' => true,
        'aliases' => [
            '@web' => getenv('PRIMARY_SITE_URL'),
        ],
    ],
];
