# Vue search guide
Yump have been doing some sort of search features for almost every project, e.g. service finder, event finder, news with filters. They may have different sorts or filters, but fundamentally they use very similar mechanism to help with the search / filtering / sorting. In order to get a feature like this starting faster, we have put a basic template with code comment in Yumply. This document is to guide you through how do we use it.

## Common feature requirements
- There's a list of items displayed to users (e.g. services, news articles, events, etc)
- Users are able to filter the results by certain filters (e.g. date range, keyword, type(s)). Some of them may be multi select.
- Users may be able to sort the results by certain criteria (e.g. sort by distance when putting in an address via Google Autocomplete)
- The user inputs are remembered in the URL as params, so users can bookmark the search, or admins can generate the links with prefilled filters.
- A load more button as the pagination control. This can also be changed to a scroll event to achieve infinite scroll.

## Demo page
/sample-vue-search

## Steps
1. Prepare your data feed
---
The data feed will eventually be requested via either:
- AJAX call to a custom controller action
- GraphQL

No matter which way you use, please make sure the data feed is formatted to a state that we are comfortable to use on the front end (e.g. no PHP objects returned).

You can take a look at the example in yumpmodule's DefaultController: actionGetNewsItems() for reference.

2. Get the front end templates and JS done
---
Templates:
Create your own xxx-list-vue.html and xxx-list-vue-item.html by duplicating: 
- /templates/_elements/sample-list-vue.html
- /templates/_elements/sample-list-vue-item.html
and include xxx-list-vue.html in the desired template.

JS:
Create your own xxx-vue-search.js by duplicating:
- /src/scripts/sample-vue-search.js
and make sure it's included in xxx-list-vue.html

Check the TODOs in those sample files for where to add your custom code